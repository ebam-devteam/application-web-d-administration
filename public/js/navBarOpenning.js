"use strict";
console.log("Script Failed Succesfully");

/** File : navBarOpenning
 * Script in charge of displaying and removing the menu when the mobile version is in use.
 */
window.onload = function () {
    document.getElementById("checkbox").onchange = function () {
        var temp = document.getElementById("menu").offsetWidth;
        var size = temp;
        if(this.checked) {
            while (temp >= 0) {
                document.getElementById("menu").style.left = -temp + "px";
                temp--;
            }
        }else {
            while (temp <= size) {
                document.getElementById("menu").style.left = -temp + "px";
                temp++;
            }
        }
    };
};

