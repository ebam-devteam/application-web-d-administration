<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id','<>',auth()->id())->get();
        return view('admin.users.index')->with([
            'users' => $users,
            'count_value' => $users->count()
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        return view('admin.settings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.edit', [
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = new User;
        $user->lastname = '';
        $user->firstname = '';
        $user->username = '';
        $user->email = '';
        $user->password = password_hash($request->password, PASSWORD_DEFAULT);
        $user->api_token = Str::random(60);
        return $this->update($request, $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if ($user == Auth::user()) {
            return view('user.settings.show', [
                'user' => $user
            ]);
        }
        return view('admin.users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if ($user == Auth::user()) {
            return view('user.settings.edit', [
                'user' => $user
            ]);
        }
        $roles = Role::all();
        return view('admin.users.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            /*'id' => 'required|unique:App\User,id',*/
            'lastname' => 'required|min:2',
            'firstname' => 'required|min:2',
            'username' => 'required|unique:App\User,username,'.$user->id,
            'email' => 'required|email',
        ]);
        $user->save();

        $user->roles()->sync($request->roles);
        $user->lastname = $request->lastname;
        $user->firstname = $request->firstname;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->save();

        return redirect()->route('admin.users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->roles()->detach();
        $user->delete();

        return redirect()->route('admin.users.index');
    }
}
