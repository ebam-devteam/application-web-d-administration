@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
    <h1>{{ trans_choice('common.member', 1)." : ".$member->firstname." ".$member->lastname }}</h1>
    </header>

    <main>
        <div class="details">
            <div>
                <p class="title">{{ trans_choice('common.member', 1) }} : </p>
                <p class="value">{{ $member->id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.lastname', 1) }} : </p>
                <p class="value">{{ $member->lastname }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.firstname', 1) }} : </p>
                <p class="value">{{ $member->firstname }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.login', 1) }} : </p>
                <p class="value">{{ $member->login }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.email', 1) }} : </p>
                <p class="value">{{ $member->email }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.address', 1) }} : </p>
                <p class="value">{{ $member->address }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.employer', 1) }} : </p>
                <p class="value">{{ $member->employer }}</p>
            </div>

            <div class="options buttons_area">
                @can('edit')
                    <a href="{{ route('user.members.edit', $member->id) }}">
                        <button class="edit">
                            @lang('buttons.edit')
                            <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                        </button>
                    </a>
                @endcan

                @can('delete')
                    <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.members.destroy', $member->id) }}">
                        @csrf
                        @method('DELETE')
                        <confirm_action text="true" src="{{ asset('images/delete_icon.svg') }}">
                        </confirm_action>
                    </form>
                @endcan
            </div>
        </div>

        <h2>{{ trans_choice('common.affected_membership', $count_value) }}</h2>

        @if ($count_value > 0)
            @include('user.memberships.table')
        @endif

        <div class="buttons_area flex">
            @can('edit')
                <a href="{{ route('user.memberships.cf.member', $member->id) }}">
                    <button class="create">
                        @lang('buttons.add')
                        <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                    </button>
                </a>
            @endcan

            @if ($count_value > 0)
                @can('read')
                    <a href="{{ route('user.memberships.export') }}">
                        <button class="export">
                            @lang('buttons.export')
                            <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                        </button>
                    </a>
                @endcan
            @endif
        </div>

    </main>
</div>
@endsection
