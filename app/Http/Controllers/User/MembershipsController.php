<?php

namespace App\Http\Controllers\User;

use App\Exports\MembershipsExport;
use App\Imports\MembershipsImport;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Member;
use App\Membership;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MembershipsController extends Controller
{
    public function validator($request)
    {
        return $request->validate(
            [
                /*'id' => 'required|unique:App\Membership,id',*/
                'member_id' => 'required',
                'invoice_id' => 'nullable',
                'date' => 'required|date',
                'type' => 'required',
                'amount' => 'required|min:0'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memberships = Membership::all();
        return view('user.memberships.index')->with([
            'memberships' => $memberships,
            'count_value' => $memberships->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.memberships.edit');
    }

    /**
     * Show the form for creating a new resource from an invoice object.
     * cf_invoice stands for Create From Invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function cf_invoice(Invoice $invoice)
    {
        $membership = new Membership;
        $membership->invoice_id = $invoice->id;
        return view('user.memberships.edit')->with([
            'membership' => $membership,
        ]);
    }

    /**
     * Show the form for creating a new resource from a member object.
     * cf_member stands for Create From Member.
     *
     * @return \Illuminate\Http\Response
     */
    public function cf_member(Member $member)
    {
        $membership = new Membership;
        $membership->member_id = $member->id;
        return view('user.memberships.edit')->with([
            'membership' => $membership,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $membership = new Membership;
        return $this->update($request, $membership);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function show(Membership $membership)
    {
        return view('user.memberships.show')->with([
            'membership' => $membership
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function edit(Membership $membership)
    {
        return view('user.memberships.edit')->with([
            'membership' => $membership,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Membership $membership)
    {
        $this->validator($request);

        $membership->member()->associate($request->member_id);
        $membership->invoice()->associate($request->invoice_id);
        $membership->date = $request->date;
        $membership->type = $request->type;
        if (isset($membership->invoice)) {
            $membership->invoice->actualize_amount($request->amount - $membership->amount);
        }
        $membership->amount = $request->amount;
        $membership->save();

        return redirect()->route('user.memberships.show', $membership->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Membership  $membership
     * @return \Illuminate\Http\Response
     */
    public function destroy(Membership $membership)
    {
        $membership->destroy($membership->id);
        return redirect()->route('user.memberships.index');
    }

    /**
     * Download the resource collection in CSV format
     */
    public function export()
    {
        return Excel::download((new MembershipsExport()), 'memberships_'.date('ymdHis').'.csv');
    }

    /**
     * Import the resource in CSV format
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function import(Request $request)
    {
        if($request->hasFile('file')) {
            try {
                $path = $request->file('file')->getRealPath();
                Excel::import(new MembershipsImport, $path, null, \Maatwebsite\Excel\Excel::CSV);
            } catch (\Exception $e) {
                \Session::flash('error', $e->getMessage());
            }
        }
        return redirect()->route('user.memberships.index');
    }
}
