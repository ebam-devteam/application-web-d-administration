<div class="details">
    <div>
        <p class="title">{{ trans_choice('common.membership', 1) }} : </p>
        <p class="value">{{ $membership->id }}</p>
    </div>
    <div>
        <p class="title">{{ trans_choice('common.member', 1) }} : </p>
        @isset ($membership->member)
            <a href="{{ route('user.members.show', $membership->member->id) }}" class="value">
                <p>
                    {{ $membership->member->firstname." ".$membership->member->lastname }}
                </p>
            </a>
        @endisset
    </div>
    @empty (($invoice ?? '') || ($receiptedInvoice ?? ''))
        <div>
            <p class="title">
                @isset ($membership->invoice_id)
                    @if (isset($membership->invoice) && $membership->invoice->receipted)
                        {{ trans_choice('common.receipted-invoice', 1) }} :
                    @else
                        {{ trans_choice('common.invoice', 1) }} :
                    @endif
                @endisset
            </p>
            <p class="value">
                @isset ($membership->invoice_id)
                    {{ $membership->invoice_id }}
                    @isset($membership->invoice)
                        <a href="{{ route('user.invoices.show', $membership->invoice_id) }}">
                            <img src="{{ asset('images/chain_icon.svg') }}" title="{{ trans_choice('common.link_to', 1, ['page' => trans_choice('common.invoice', 1)]) }}" alt="Link icon">
                        </a>
                    @endisset
                @endisset
            </p>
        </div>
    @endempty
    <div>
        <p class="title">{{ trans_choice('common.date', 1) }} : </p>
        <p class="value">{{ $membership->date }}</p>
    </div>
    <div>
        <p class="title">{{ trans_choice('common.type', 1) }} : </p>
        <p class="value">{{ $membership->type }}</p>
    </div>
    <div>
        <p class="title">{{ trans_choice('common.amount', 1) }} : </p>
        <p class="value">{{ $membership->amount }}</p>
    </div>


    <div class="options buttons_area">
        @can('edit')
            <a href="{{ route('user.memberships.edit', $membership->id) }}">
                <button class="edit">
                    @lang('buttons.edit')
                    <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                </button>
            </a>
        @endcan

        @can('delete')
            <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.memberships.destroy', $membership->id) }}">
                @csrf
                @method('DELETE')
                <confirm_action text="true" src="{{ asset('images/delete_icon.svg') }}">
                </confirm_action>
            </form>
        @endcan
    </div>
</div>
