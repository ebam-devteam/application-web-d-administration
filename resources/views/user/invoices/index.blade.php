@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @if (Request::is('user/invoices'))
                {{ trans_choice('common.invoice', 2) }}
            @else
                {{ trans_choice('common.receipted-invoice', 2) }}
            @endif
        </h1>
    </header>

    <main>
        <input type="text" id="inputFilter" placeholder="@lang('common.research')" title="@lang('common.type_here')">

        <div class="container_table">
            <div class="table" id="table">
                <div class="row header">
                    <div class="cell short">
                        <p>{{ trans_choice('common.id', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.date', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.total_amount', $count_value) }}</p>
                    </div>
                    <div class="cell large">
                        <p>{{ trans_choice('common.payment-mean', $count_value) }}</p>
                    </div>
                    <div class="cell large">
                        <p>{{ trans_choice('common.payment-address', $count_value) }}</p>
                    </div>
                    <div class="cell short">
                        <p>{{ trans_choice('common.payment_id', $count_value) }}</p>
                    </div>
                    <div class="cell large">
                        <p>{{ trans_choice('common.signatory_name', $count_value) }}</p>
                    </div>
                    @can('edit', 'read', 'delete')
                        <div class="cell options">
                            <p>{{ trans_choice('common.action', 2) }}</p>
                        </div>
                    @endcan
                </div>
                @foreach ($invoices as $invoice)
                    <div class="row">
                        <a href="{{ route('user.invoices.show', $invoice->id)}}">
                            <div class="cell short" data-title="{{ trans_choice('common.invoice', 1) }}">
                                <p>{{ $invoice->id}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.date', 1) }}">
                                <p>{{ $invoice->date}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.total_amount', 1) }}">
                                <p>{{ $invoice->total_amount}}</p>
                            </div>
                            <div class="cell large" data-title="{{ trans_choice('common.payment-mean', 1) }}">
                                <p>{{ $invoice->payment_mean}}</p>
                            </div>
                            <div class="cell large" data-title="{{ trans_choice('common.payment-address', 1) }}">
                                <p>{{ $invoice->payment_address}}</p>
                            </div>
                            <div class="cell short" data-title="{{ trans_choice('common.payment_id', 1) }}">
                                <p>{{ $invoice->payment_id}}</p>
                            </div>
                            <div class="cell large" data-title="{{ trans_choice('common.signatory_name', 1) }}">
                                <p>{{ $invoice->signatory_name}}</p>
                            </div>
                        </a>

                        @can('edit', 'read', 'delete')
                            <div class="cell options">
                                <div class="container-icon">
                                    @can('edit')
                                        <form class="ico" method="GET" accept-charset="utf-8" action="
                                            @if (Request::is('user/invoices'))
                                                {{ route('user.invoices.edit', $invoice->id) }}
                                            @else
                                                {{ route('user.receipted-invoices.edit', $invoice->id) }}
                                            @endif">
                                            @csrf
                                            <button type="submit" >
                                                <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                                            </button>
                                        </form>
                                    @endcan

                                    @can('read')
                                        @if ($invoice->memberships->first())
                                            <form class="ico" method="GET" accept-charset="utf-8" target="_blank" action="
                                                @if (Request::is('user/invoices'))
                                                    {{ route('user.invoices.download', $invoice->id) }}
                                                @else
                                                    {{ route('user.receipted-invoices.download', $invoice->id) }}
                                                @endif">
                                                @csrf
                                                <button type="submit">
                                                    <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                                                </button>
                                            </form>
                                        @endif
                                    @endcan

                                    @can('delete')
                                        <form class="ico" method="POST" accept-charset="utf-8" action="
                                            @if (Request::is('user/invoices'))
                                                {{ route('user.invoices.destroy', $invoice->id) }}
                                            @else
                                                {{ route('user.receipted-invoices.destroy', $invoice->id) }}
                                            @endif">
                                            @csrf
                                            @method('DELETE')
                                            <confirm_action src="{{ asset('images/delete_icon.svg') }}">
                                            </confirm_action>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        @endcan
                    </div>
                @endforeach
            </div>
        </div>

        <div class="buttons_area">
            @can('edit')
                <a href="
                    @if (Request::is('user/invoices'))
                        {{ route('user.invoices.create') }}
                    @else
                        {{ route('user.receipted-invoices.create') }}
                    @endif">
                    <button class="create">
                        @lang('buttons.add')
                        <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                    </button>
                </a>

                <div class="file_color">
                    <form method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <button class="import no-margin" type="submit" formaction="
                        @if (Request::is('user/invoices'))
                            {{ route('user.invoices.import') }}
                        @else
                            {{ route('user.receipted-invoices.import') }}
                        @endif">
                            @lang('buttons.import')
                            <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                        </button>
                    </form>
                </div>
            @endcan

            @can('read')
                <a href="
                    @if (Request::is('user/invoices'))
                        {{ route('user.invoices.export') }}
                    @else
                        {{ route('user.receipted-invoices.export') }}
                    @endif">
                    <button class="export">
                        @lang('buttons.export')
                        <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                    </button>
                </a>
            @endcan
        </div>

    </main>
</div>
@endsection
