@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($member)
                {{ trans_choice('common.member', 1)." : ".$member->firstname." ".$member->lastname }}
            @else
                @lang('common.member_creation')
            @endisset
        </h1>
    </header>

    <main>
        <div class="db_infos">
            <form class="buttons_area" method="POST" accept-charset="utf-8">
                @csrf
                @if (Request::is('user/members/*/edit'))
                    @method('PATCH')
                @endif

                <div class="case_infos">
                    <label for="lastname" class="title">{{ trans_choice('common.lastname', 1) }} : </label>
                    <input type="text" id="lastname" name="lastname" class="@error('lastname') is-require @enderror" value="{{ old('lastname', $member->lastname ?? '') }}" maxlength="256" required>
                </div>
                @error('lastname')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="firstname" class="title">{{ trans_choice('common.firstname', 1) }} : </label>
                    <input type="text" id="firstname" name="firstname" class="@error('firstname') is-require @enderror" value="{{ old('firstname', $member->firstname ?? '') }}" maxlength="256" required>
                </div>
                @error('firstname')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="login" class="title">{{ trans_choice('common.login', 1) }} : </label>
                    <input type="text" id="login" name="login" value="{{ old('login', $member->login ?? '') }}" maxlength="256">
                </div>

                <div class="case_infos">
                    <label for="email" class="title">{{ trans_choice('common.email', 1) }} : </label>
                    <input type="text" id="email" name="email" value="{{ old('email', $member->email ?? '') }}" maxlength="256">
                </div>

                <div class="case_infos">
                    <label for="address" class="title">{{ trans_choice('common.address', 1) }} : </label>
                    <input type="text" id="address" name="address" value="{{ old('address', $member->address ?? '') }}" maxlength="256">
                </div>

                <div class="case_infos">
                    <label for="employer" class="title">{{ trans_choice('common.employer', 1) }} : </label>
                    <input type="text" id="employer" name="employer" value="{{ old('employer', $member->employer ?? '') }}" maxlength="256">
                </div>

                <div>
                    <button type="submit" class="save" formaction="
                        @if (Request::is('user/members/*/edit'))
                            {{ route('user.members.update', $member->id) }}
                        @else
                            {{ route('user.members.store') }}
                        @endif">
                        @lang('buttons.save')
                    </button>
                </div>
            </form>

            <form class="buttons_area" method="GET" accept-charset="utf-8">
                @csrf
                <button type="submit" class="cancel" formaction="
                    @if (Request::is('user/members/*/edit'))
                        {{ route('user.members.show', $member->id) }}
                    @else
                        {{ route('user.members.index') }}
                    @endif">
                    @lang('buttons.cancel')
                </button>
            </form>

        </div>
    </main>
</div>
@endsection
