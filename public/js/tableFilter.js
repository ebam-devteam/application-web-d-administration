window.addEventListener("load", function () {
    let input;
    input = document.querySelector("#inputFilter");

    input.addEventListener("keyup", () => {
        let cells, filter, isValid, rows, txtValue;

        filter = input.value.toUpperCase();

        rows = Array.from(document.querySelectorAll("#table .row:not(.header)"));

        rows.forEach(row => {
            cells = Array.from(row.querySelectorAll(".cell"));
            cells.pop();

            isValid = false;
            cells.forEach(cell => {
                txtValue = cell.querySelector("p").textContent;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    isValid = true;
                }
            });
            if (isValid) {
                row.style.display = "";
            } else {
                row.style.display = "none";
            }
        });
    });
});
