<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Auth;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('user.settings.show')->with('user', Auth::user());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $roles = Role::all();
        return view('user.settings.edit', [
            'user' => Auth::user()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            /*'id' => 'required|unique:App\User,id',*/
            'lastname' => 'required',
            'firstname' => 'required',
            'username' => 'required|unique:App\User,username,'.$user->id,
            'email' => 'required|email',
            'password' => 'nullable|string|min:8|confirmed'
        ]);

        $user->lastname = $request->lastname;
        $user->firstname = $request->firstname;
        $user->username = $request->username;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = password_hash($request->password, PASSWORD_DEFAULT);
        }
        $user->save();

        return redirect()->route('user.settings.show');
    }
}
