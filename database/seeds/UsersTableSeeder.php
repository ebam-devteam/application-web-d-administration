<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
Use Illuminate\Support\Facades\Hash;
Use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->delete();

        $admin = User::create([
            'lastname' => 'admin',
            'firstname' => 'admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'api_token' => Str::random(60)
        ]);

        $membre = User::create([
            'lastname' => 'membre',
            'firstname' => 'membre',
            'username' => 'membre',
            'email' => 'membre@membre.com',
            'password' => Hash::make('password'),
            'api_token' => Str::random(60)
        ]);

        $adminRole = Role::where('name', 'admin')->first();
        $membreRole = Role::where('name', 'membre')->first();

        $admin->roles()->attach($adminRole);
        $membre->roles()->attach($membreRole);
    }
}
