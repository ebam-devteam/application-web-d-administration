<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\DatalistPaymentMeans as DatalistPaymentMeansResource;
use App\DatalistPaymentMean;
use Illuminate\Http\Request;

class DatalistPaymentMeansController extends Controller
{
    /**
     *
     */
    public function index()
    {
      return DatalistPaymentMeansResource::collection(DatalistPaymentMean::orderBy('name')->get());
    }

    /**
     *
     * @param  \App\DatalistPaymentMean  $datalistPaymentMean
     */
    public function show(DatalistPaymentMean $datalistPaymentMean)
    {
      return new DatalistPaymentMeansResource($datalistPaymentMean);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
      $datalistPaymentMean = DatalistPaymentMean::create([
        'name' => $request->name,
      ]);
      return new DatalistPaymentMeansResource($datalistPaymentMean);
    }


    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatalistPaymentMean  $datalistPaymentMean
     */
    public function update(Request $request, DatalistPaymentMean $datalistPaymentMean)
    {
        $datalistPaymentMean->update($request->only(['name']));
        return new DatalistPaymentMeansResource($datalistPaymentMean);
    }

    /**
     *
     * @param  \App\DatalistPaymentMean  $datalistPaymentMean
     */
    public function destroy(DatalistPaymentMean $datalistPaymentMean)
    {
      $datalistPaymentMean->delete();
      return response()->json(null, 204);
    }
}
