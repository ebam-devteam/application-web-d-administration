<?php

use App\Designation;
use App\PurchaseOrder;
use Illuminate\Database\Seeder;

class DesignationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Designation::truncate();

        Designation::create([
            'purchase_order_id' => 512,
            'name' => "Achat écrans",
            'unit_price' => 200,
            'quantity' => 25,
            'total_amount' => 400,
            'note' => "Nouveaux écrans de travail"
        ])->purchase_order()->associate(PurchaseOrder::find(512));
    }
}
