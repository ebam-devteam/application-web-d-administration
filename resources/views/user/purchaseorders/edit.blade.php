@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($purchaseOrder)
                {{ trans_choice('common.purchase-order', 1)." : ".$purchaseOrder->id }}
            @else
                @lang('common.purchaseorder_creation')
            @endisset
        </h1>
    </header>

    <main>
        <div class="db_infos">
            <form class="buttons_area" method="POST" accept-charset="utf-8">
                @csrf
                @if (Request::is('user/purchase-orders/*/edit'))
                    @method('PATCH')
                @endif

                <div class="case_infos">
                    <label for="provider" class="title">{{ trans_choice('common.provider', 1) }} : </label>
                    <input type="text" id="provider" name="provider" class="@error('provider') is-require @enderror" value="{{ old('provider', $purchaseOrder->provider ?? '') }}" maxlength="256" required>
                </div>
                @error('provider')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="total_amount" class="title">{{ trans_choice('common.total_amount', 1) }} : </label>
                    <input type="number" min="0" max="999999.99" step="0.01" id="total_amount" name="total_amount" value="{{ old('total_amount', $purchaseOrder->total_amount ?? '') }}">
                </div>
                @error('total_amount')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="VAT" class="VAT">{{ trans_choice('common.VAT', 1) }} : </label>
                    <input type="number" min="0" max="999999.99" step="0.01" id="VAT" name="VAT" value="{{ old('VAT', $purchaseOrder->VAT ?? '') }}">
                </div>
                @error('VAT')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="date" class="title">{{ trans_choice('common.date', 1) }} : </label>
                    <input type="date" id="date" name="date" value="{{ old('date', $purchaseOrder->date ?? date('Y-m-d')) }}">
                </div>
                @error('date')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="signatory_type" class="title">{{ trans_choice('common.signatory_type', 1) }} : </label>
                    <input type="text" id="signatory_type" name="signatory_type" value="{{ old('signatory_type', $purchaseOrder->signatory_type ?? '') }}" maxlength="256">
                </div>
                @error('signatory_type')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="signatory_name" class="title">{{ trans_choice('common.signatory_name', 1) }} : </label>
                    <input type="text" id="signatory_name" name="signatory_name" value="{{ old('signatory_name', $purchaseOrder->signatory_name ?? '') }}" maxlength="256">
                </div>
                @error('signatory_name')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="signatory_city" class="title">{{ trans_choice('common.signatory_city', 1) }} : </label>
                    <input type="text" id="signatory_city" name="signatory_city" value="{{ old('signatory_city', $purchaseOrder->signatory_city ?? '') }}" maxlength="256">
                </div>
                @error('signatory_city')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div>
                    <button type="submit" class="save" formaction="
                        @if (Request::is('user/purchase-orders/*/edit'))
                            {{ route('user.purchase-orders.update', $purchaseOrder->id) }}
                        @else
                            {{ route('user.purchase-orders.store') }}
                        @endif">
                        @lang('buttons.save')
                    </button>
                </div>
            </form>

            <form class="buttons_area" method="GET" accept-charset="utf-8">
                @csrf
                <button type="submit" class="cancel" formaction="
                    @if (Request::is('user/purchase-orders/*/edit'))
                        {{ route('user.purchase-orders.show', $purchaseOrder->id) }}
                    @else
                        {{ route('user.purchase-orders.index') }}
                    @endif">
                    @lang('buttons.cancel')
                </button>
            </form>

        </div>
    </main>
</div>
@endsection
