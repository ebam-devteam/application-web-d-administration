@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($designation->id)
                {{ trans_choice('common.designation', 1)." : ".$designation->id }}
                {{--var_dump($designation)--}}
            @else
                @lang('common.designation_creation')
            @endisset
        </h1>
    </header>

    <main>
        <div class="db_infos">
            <form class="buttons_area" method="POST" accept-charset="utf-8">
                @csrf
                @if (Request::is('user/designations/*/edit'))
                    @method('PATCH')
                @endif

                <div class="case_infos">
                    <label for="purchase_order_id" class="title">{{ trans_choice('common.purchase-order_id', 1) }} : </label>
                    <input type="number" step="1" max="999999.99" id="purchase_order_id" name="purchase_order_id" class="@error('purchase_order_id') is-require @enderror" value="{{ old('provider', $designation->purchase_order_id ?? '') }}" required>
                </div>
                @error('purchase_order_id')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="name" class="title">{{ trans_choice('common.name', 1) }} : </label>
                    <input type="text" id="name" name="name" value="{{ old('name', $designation->name ?? '') }}" maxlength="256">
                </div>
                @error('name')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="unit_price" class="unit_price">{{ trans_choice('common.unit_price', 1) }} : </label>
                    <input type="number" min="0" max="999999.99" step="0.01" id="unit_price" name="unit_price" value="{{ old('unit_price', $designation->unit_price ?? '') }}">
                </div>
                @error('unit_price')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="quantity" class="title">{{ trans_choice('common.quantity', 1) }} : </label>
                    <input type="number" min="1" max="999999" id="quantity" name="quantity" value="{{ old('quantity', $designation->quantity ?? '')}}">
                </div>
                @error('quantity')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="total_amount" class="title">{{ trans_choice('common.total_amount', 1) }} : </label>
                    <input type="number" min="0" max="999999.99" step="0.01" id="total_amount" name="total_amount" value="{{ old('total_amount', $designation->total_amount ?? '') }}">
                </div>
                @error('total_amount')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror


                <div class="case_infos">
                    <label for="note" class="title">{{ trans_choice('common.note', 1) }} : </label>
                    <textarea name="note" id="note" cols="30" rows="10" maxlength="256">{{ old('note', $designation->note ?? '') }}</textarea>
                </div>
                @error('note')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div>
                    <button type="submit" class="save" formaction="
                        @if (Request::is('user/designations/*/edit'))
                            {{ route('user.designations.update', $designation->id) }}
                        @else
                            {{ route('user.designations.store') }}
                        @endif">
                        @lang('buttons.save')
                    </button>
                </div>
            </form>

           <form class="buttons_area" method="GET" accept-charset="utf-8">
                @csrf
                <button type="submit" class="cancel" formaction="
                    @if (Request::is('user/designations/*/edit'))
                        {{ route('user.designations.show', $designation->id) }}
                    @else
                        {{ route('user.purchase-orders.show', $designation->purchase_order_id) }}
                    @endif">
                    @lang('buttons.cancel')
                </button>
            </form>

        </div>
    </main>
</div>
@endsection
