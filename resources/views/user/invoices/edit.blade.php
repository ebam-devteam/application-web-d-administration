@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($invoice)
                @if ($invoice->receipted)
                    {{ trans_choice('common.edition_of', 1, ['object' => trans_choice('common.receipted-invoice', 1)]) }}
                @else
                    {{ trans_choice('common.edition_of', 1, ['object' => trans_choice('common.invoice', 1)]) }}
                @endif
                {{ " : ".$invoice->id }}
            @else
                @if (Request::is('user/invoices/*'))
                    @lang('common.invoice_creation')
                @else
                    @lang('common.receipted-invoice_creation')
                @endif
            @endisset
        </h1>
    </header>

    <main>
        <div class="db_infos">
            <form class="buttons_area" method="POST" accept-charset="utf-8">
                @csrf
                @if (Request::is('user/invoices/*/edit') || Request::is('user/receipted-invoices/*/edit'))
                    @method('PATCH')
                @endif

                <div class="case_infos">
                    <label for="date" class="title">{{ trans_choice('common.date', 1) }} : </label>
                    <input type="date" id="date" name="date" class="@error('date') is-require @enderror" value="{{ old('date', $invoice->date ?? date('Y-m-d')) }}" required>
                </div>
                @error('date')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="total_amount" class="title">{{ trans_choice('common.total_amount', 1) }} : </label>
                    <input type="text" id="total_amount" name="total_amount" class="@error('total_amount') is-require @enderror" value="{{ old('total_amount', $invoice->total_amount ?? '') }}" maxlength="256" required>
                </div>
                @error('total_amount')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <autocomplete_data_payment
                  error="@error('payment_mean_id') is-require @enderror"
                  label="{{ trans_choice('common.payment-mean', 1) }}"
                  resource="payment_mean"
                  value="{{ old('payment_mean_id', $invoice->payment_mean ?? '') }}"
                  api_token="{{ Auth::user()->api_token }}">
                </autocomplete_data_payment>
                @error('payment_mean_id')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="payment_address" class="title">{{ trans_choice('common.payment-address', 1) }} : </label>
                    <input type="text" id="payment_address" name="payment_address" class="@error('payment_address') is-require @enderror" value="{{ old('payment_address', $invoice->payment_address ?? '') }}" maxlength="256" required>
                </div>
                @error('payment_address')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="payment_id" class="title">{{ trans_choice('common.payment_id', 1) }} : </label>
                    <input type="text" id="payment_id" name="payment_id" value="{{ old('payment_id', $invoice->payment_id ?? '') }}" maxlength="256">
                </div>

                <div class="case_infos">
                    <label for="signatory_type" class="title">{{ trans_choice('common.signatory_type', 1) }} : </label>
                    <input type="text" id="signatory_type" name="signatory_type" class="@error('signatory_type') is-require @enderror" value="{{ old('signatory_type', $invoice->signatory_type ?? '') }}" maxlength="256" required>
                </div>
                @error('signatory_type')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="signatory_name" class="title">{{ trans_choice('common.signatory_name', 1) }} : </label>
                    <input type="text" id="signatory_name" name="signatory_name" class="@error('signatory_name') is-require @enderror" value="{{ old('signatory_name', $invoice->signatory_name ?? '') }}" maxlength="256" required>
                </div>
                @error('signatory_name')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="signatory_city" class="title">{{ trans_choice('common.signatory_city', 1) }} : </label>
                    <input type="text" id="signatory_city" name="signatory_city" class="@error('signatory_city') is-require @enderror" value="{{ old('signatory_city', $invoice->signatory_city ?? '') }}" maxlength="256" required>
                </div>
                @error('signatory_city')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div>
                    <button type="submit" class="save" formaction="
                        @if (Request::is('user/invoices/*/edit') || Request::is('user/receipted-invoices/*/edit'))
                            @if ($invoice->receipted)
                                {{ route('user.receipted-invoices.update', $invoice->id) }}
                            @else
                                {{ route('user.invoices.update', $invoice->id) }}
                            @endif
                        @else
                            @if (Request::is('user/invoices/*'))
                                {{ route('user.invoices.store') }}
                            @else
                                {{ route('user.receipted-invoices.store') }}
                            @endif
                        @endif">
                        @lang('buttons.save')
                    </button>
                </div>
            </form>

            <form class="buttons_area" method="GET" accept-charset="utf-8">
                @csrf
                <button type="submit" class="cancel" formaction="
                    @if (Request::is('user/invoices/*/edit') || Request::is('user/receipted-invoices/*/edit'))
                        @if ($invoice->receipted)
                            {{ route('user.receipted-invoices.show', $invoice->id) }}
                        @else
                            {{ route('user.invoices.show', $invoice->id) }}
                        @endif
                    @else
                        @if (Request::is('user/invoices/*'))
                            {{ route('user.invoices.index') }}
                        @else
                            {{ route('user.receipted-invoices.index') }}
                        @endif
                    @endif">
                    @lang('buttons.cancel')
                </button>
            </form>

        </div>
    </main>
</div>
@endsection
