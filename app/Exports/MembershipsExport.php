<?php

namespace App\Exports;

use App\Membership;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class MembershipsExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Membership::all();
    }

    /**
     * @return array names of the header
     */
    public function headings(): array
    {
        return [
            'id',
            'member_id',
            'invoice_id',
            'date',
            'type',
            'amount',
            'created_at',
            'updated_at'
        ];
    }
}
