<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')

    <!-- Scripts -->
    <script type="text/javascript" src="{{asset('js/navBarOpenning.js')}}"></script>
    @yield('javascript')

    <!-- Styles -->
    <link href="{{ asset('css/default.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    <div class="container_all">

        {{--
        <header class="row">
            @include('includes.header')
        </header>
        --}}

        @auth
            @include('layouts.nav')
        @endauth

        @yield('content')

        {{--
        <footer class="row">
            @include('includes.footer')
        </footer>
        --}}

    </div>
</body>
</html>
