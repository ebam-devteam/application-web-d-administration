var curCol = 0;
var ASC = true;

window.addEventListener("load", function () {

    let columns = Array.from(document.querySelectorAll("#table .row.header .cell"));
    columns.pop();

    columns.forEach(cell => {
        cell.addEventListener("click", function () {
            let index = columns.indexOf(cell);
            if (index == curCol) {
                ASC = !ASC;
            } else {
                ASC = true;
            }

            sortTable(index, ASC);

            columns.forEach(col => {
                let img = col.querySelector("img");
                if (img != undefined) {
                    col.removeChild(img);
                }
            });


            let img = document.createElement("img");
            if (ASC) {
                img.setAttribute("src", "/images/caret-up_white.svg");
                img.setAttribute("alt", "fleche haute");
                cell.appendChild(img);
            } else {
                img.setAttribute("src", "/images/caret-down_white.svg");
                img.setAttribute("alt", "fleche basse");
                cell.appendChild(img);
            }

            curCol = index;
        });
    });
})

function sortTable(index, order) {
    let rows, switching, i, x, y, shouldSwitch;
    switching = true;
    while (switching) {
        switching = false;
        rows = Array.from(document.querySelectorAll("#table .row"));
        rows.shift();
        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].querySelectorAll(".cell")[index].childNodes[0].textContent;
            y = rows[i + 1].querySelectorAll(".cell")[index].childNodes[0].textContent;
            if (order) {
                if (x.toLowerCase() > y.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else {
                if (x.toLowerCase() < y.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}
