<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->unsigned();
            $table->bigInteger('invoice_id')->unsigned()->nullable();
            $table->date('date');
            $table->string('type', 256);
            $table->float('amount', 8, 2);
            $table->timestamps();

            $table->foreign('member_id', 'FK_member_id')->references('id')->on('members')->onDelete('cascade');
            $table->foreign('invoice_id', 'FK_invoice_id')->references('id')->on('invoices')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
