@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($user)
                {{ trans_choice('common.user_account', 1).' : '.$user->firstname.' '.$user->lastname }}
            @else
                @lang('common.user_creation')
            @endisset
        </h1>
    </header>

    @can('manage')
        @include('admin.tabs')
    @endcan

    <main>
        <div class="db_infos">
            <form class="buttons_area" method="POST" accept-charset="utf-8">
                @csrf
                @if (Request::is('admin/users/*/edit'))
                    @method('PATCH')
                @endif

                <div class="case_infos">
                    <label for="lastname" class="title">{{ trans_choice('common.lastname', 1) }} : </label>
                    <input type="text" id="lastname" name="lastname" class="@error('lastname') is-require @enderror" value="{{ old('lastname', $user->lastname ?? '') }}" maxlength="256" required>
                </div>
                @error('lastname')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="firstname" class="title">{{ trans_choice('common.firstname', 1) }} : </label>
                    <input type="text" id="firstname" name="firstname" class="@error('firstname') is-require @enderror" value="{{ old('firstname', $user->firstname ?? '') }}" maxlength="256" required>
                </div>
                @error('firstname')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="username" class="title">{{ trans_choice('common.username', 1) }} : </label>
                    <input type="text" id="username" name="username" class="@error('username') is-require @enderror" value="{{ old('username', $user->username ?? '') }}" maxlength="128" required>
                </div>
                @error('username')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="email" class="title">{{ trans_choice('common.email', 1) }} : </label>
                    <input type="email" id="email" name="email" class="@error('email') is-require @enderror" value="{{ old('email', $user->email ?? '') }}" required>
                </div>
                @error('email')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    @foreach ($roles as $role)
                        <div>
                            <input type="checkbox" name="roles[]" value="{{ $role->id }}" id="{{ $role->id }}"
                                @if (isset($user) && $user->roles->pluck('id')->contains($role->id)) checked @endif>
                            <label for="{{ $role->id }}">{{ $role->name }}</label>
                        </div>
                    @endforeach
                </div>
                @error('email')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="password" class="title">{{ __('New Password') }} : </label>
                    <input type="password" id="password" name="password" class="@error('password') is-require @enderror" value="{{ old('password') }}" maxlength="256">
                </div>
                @error('password')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="password_confirmation" class="title">{{ __('Confirm New Password') }} : </label>
                    <input type="password" id="password_confirmation" name="password_confirmation" class="@error('password_confirmation') is-require @enderror" value="{{ old('password_confirmation') }}" maxlength="256">
                </div>
                @error('password_confirmation')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div>
                    <button type="submit" class="save" formaction="
                        @if (Request::is('admin/users/*/edit'))
                            {{ route('admin.users.update', $user->id) }}
                        @else
                            {{ route('admin.users.store') }}
                        @endif">
                        @lang('buttons.save')
                    </button>
                </div>
            </form>

            <form class="buttons_area" method="GET" accept-charset="utf-8">
                @csrf
                <button type="submit" class="cancel" formaction="
                    @if (Request::is('admin/users/*/edit'))
                        {{ route('admin.users.show', $user->id) }}
                    @else
                        {{ route('admin.users.index') }}
                    @endif">
                    @lang('buttons.cancel')
                </button>
            </form>
        </div>
    </main>
</div>
@endsection
