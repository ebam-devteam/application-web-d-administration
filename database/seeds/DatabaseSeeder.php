<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MembersTableSeeder::class);
        $this->call(InvoicesTableSeeder::class);
        $this->call(MembershipsTableSeeder::class);
        $this->call(PurchaseOrdersTableSeeder::class);
        $this->call(DesignationsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DatalistMembershipsTableSeeder::class);
        $this->call(DatalistPaymentMeansTableSeeder::class);
    }
}
