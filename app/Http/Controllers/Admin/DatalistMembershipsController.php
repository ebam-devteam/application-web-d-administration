<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\DatalistMemberships as DatalistMembershipsResource;
use App\DatalistMemberships;
use Illuminate\Http\Request;

class DatalistMembershipsController extends Controller
{
    /**
     *
     */
    public function index()
    {
      return DatalistMembershipsResource::collection(DatalistMemberships::orderBy('type')->get());
    }

    /**
     *
     * @param  \App\DatalistMemberships  $datalistMembership
     */
    public function show(DatalistMemberships $datalistMembership)
    {
      return new DatalistMembershipsResource($datalistMembership);
    }

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
      $datalistMembership = DatalistMemberships::create([
        'type' => $request->type,
        'amount' => $request->amount,
      ]);
      return new DatalistMembershipsResource($datalistMembership);
    }


    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatalistMemberships  $datalistMembership
     */
    public function update(Request $request, DatalistMemberships $datalistMembership)
    {
        $datalistMembership->update($request->only(['type', 'amount']));
        return new DatalistMembershipsResource($datalistMembership);
    }

    /**
     *
     * @param  \App\DatalistMemberships  $datalistMembership
     */
    public function destroy(DatalistMemberships $datalistMembership)
    {
      $datalistMembership->delete();
      return response()->json(null, 204);
    }
}
