@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($membership->member)
                {{ trans_choice('common.membership_of', 1, ['user' => $membership->member->firstname." ".$membership->member->lastname]) }}
            @else
                @lang('common.member_deleted')
            @endisset
        </h1>
    </header>

    <main>
        @include('user.memberships.details')
    </main>
</div>
@endsection
