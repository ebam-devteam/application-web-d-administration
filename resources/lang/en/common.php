<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on many pages for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'example' => 'This sentence is the value for the key `example`.',
    'action' => '{1} Action|[2,*] Actions',
    'address' => '{1} Address|[2,*] Addresses',
    'affected_membership' => '{0} No membership affected|{1} Affected Membership|[2,*] Affected Memberships',
    'amount' => '{1} Amount|[2,*] Amounts',
    'date' => '{1} Date|[2,*] Dates',
    'edition_of' => 'Edition of :object',
    'email' => '{1} Email|[2,*] Emails',
    'employer' => '{1} Employer|[2,*] Employers',
    'firstname' => '{1} Firstname|[2,*] Firstnames',
    'home' => 'Home',
    'id' => '#',
    'invoice' => '{1} Invoice|[2,*] Invoices',
    'lastname' => '{1} Lastname|[2,*] Lastnames',
    'link_to' => 'Link to :page',
    'login' => '{1} Login|[2,*] Logins',
    'logout' => 'Logout',
    'member' => '{1} Member|[2,*] Members',
    'member_creation' => 'Member Creation',
    'member_deleted' => 'Member Deleted',
    'membership' => '{1} Membership|[2,*] Memberships',
    'membership_creation' => 'Membership Creation',
    'membership_of' => '{1} Membership of :user|[2,*] Memberships of :user',
    'menu' => 'Menu',
    'payment-address' => '{1} Payment Address|[2,*] Payment Addresses',
    'payment-mean' => '{1} Payment Mean|[2,*] Payment Means',
    'purchase-order' => '{1} Purchase Order|[2,*] Purchase Orders',
    'receipted-invoice' => '{1} Receipted Invoice|[2,*] Receipted Invoices',
    'research' => 'Research',
    'setting' => '{1} Setting|[2,*] Settings',
    'total_amount' => 'Total Amount',
    'type' => '{1} Type|[2,*] Types',
    'type' => 'Type Here',

];
