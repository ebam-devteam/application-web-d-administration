<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scholarship extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'firstName',
        'lastName',
        'email',
        'password',
        'laboratory',
        'whoPaid',
        'refund',
    ];

    public function files() {
        return $this->hasMany('App\Files');
    }
}
