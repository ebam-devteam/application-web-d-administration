<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'member_id',
        'invoice_id',
        'date',
        'type',
        'amount'
    ];

    public function member() {
        return $this->belongsTo('App\Member');
    }

    public function invoice() {
        return $this->belongsTo('App\Invoice');
    }
}
