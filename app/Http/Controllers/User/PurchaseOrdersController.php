<?php

namespace App\Http\Controllers\User;

use App\Exports\PurchaseOrdersExport;
use App\Imports\PurchaseOrdersImport;
use App\Http\Controllers\Controller;
use App\PurchaseOrder;
use App\Designation;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class PurchaseOrdersController extends Controller
{
    public function validator($request)
    {
        return $request->validate(
            [
                'provider' => 'required',
                'total_amount' => 'required',
                'VAT' => 'required',
                'date' => 'required',
                'signatory_type' => 'required',
                'signatory_name' => 'required',
                'signatory_city' => 'required'
            ]
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $purchaseOrders = PurchaseOrder::all();
        return view('user.purchaseorders.index')->with([
            'purchaseOrders' => $purchaseOrders,
            'count_value' => $purchaseOrders->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.purchaseorders.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $purchaseOrder = new PurchaseOrder;
        return $this->update($request, $purchaseOrder);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function show(PurchaseOrder $purchaseOrder)
    {
        $designations = Designation::where('purchase_order_id', $purchaseOrder->id)->get();

        return view('user.purchaseorders.show')->with([
            'purchaseOrder' => $purchaseOrder,
            'designations' => $designations,
            'count_value' => $designations->count()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $purchaseOrder)
    {
        return view('user.purchaseorders.edit')->with([
            'purchaseOrder' => $purchaseOrder,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PurchaseOrder $purchaseOrder)
    {
        $this->validator($request);

        $purchaseOrder->provider = $request->provider;
        $purchaseOrder->total_amount = $request->total_amount;
        $purchaseOrder->VAT = $request->VAT;
        $purchaseOrder->date = $request->date;
        $purchaseOrder->signatory_type = $request->signatory_type;
        $purchaseOrder->signatory_name = $request->signatory_name;
        $purchaseOrder->signatory_city = $request->signatory_city;
        $purchaseOrder->save();

        return redirect()->route('user.purchase-orders.show', $purchaseOrder->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PurchaseOrder  $purchaseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $purchaseOrder)
    {
        foreach ($purchaseOrder->designations as $designation) {
            $designation->destroy($designation->id);
        }
        $purchaseOrder->destroy($purchaseOrder->id);
        return redirect()->route('user.purchase-orders.index');
    }

    /**
     * Download the resource collection in CSV format
     */
    public function export()
    {
        return Excel::download((new PurchaseOrdersExport()), 'purchase-orders_'.date('ymdHis').'.csv');
    }

    /**
     * Import the resource in CSV format
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function import(Request $request)
    {
        if($request->hasFile('file')) {
            try {
                $path = $request->file('file')->getRealPath();
                Excel::import(new PurchaseOrdersImport, $path, null, \Maatwebsite\Excel\Excel::CSV);
            } catch (\Exception $e) {
                \Session::flash('error', $e->getMessage());
            }
        }
        return redirect()->route('user.purchase-orders.index');
    }
}
