<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'provider',
        'total_amount',
        'VAT',
        'date',
        'signatory_type',
        'signatory_name',
        'signatory_city'
    ];

    public function designations() {
        return $this->hasMany('App\Designation');
    }
}
