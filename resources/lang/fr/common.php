<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Home Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on home page for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'example' => 'Cette phrase est la valeur pour la clé `example`.',
    'action' => '{1} Action|[2,*] Actions',
    'address' => '{1} Adresse|[2,*] Adresses',
    'affected_designation' => '{0} Pas de désignation affectée|{1} Désignation affectée|[2,*] Désignations affectées',
    'affected_membership' => '{0} Pas d\'adhésion affectée|{1} Adhésion affectée|[2,*] Adhésions affectées',
    'amount' => '{1} Montant|[2,*] Montants',
    'date' => '{1} Date|[2,*] Dates',
    'designation' => '{1} Désignation|[2,*] Désignations',
    'designation_creation' => 'Création d\'une désignation',
    'edition_of' => 'Édition de :object',
    'email' => '{1} Email|[2,*] Emails',
    'employer' => '{1} Employeur|[2,*] Employeurs',
    'firstname' => '{1} Prénom|[2,*] Prénoms',
    'home' => 'Accueil',
    'id' => '#',
    'invoice' => '{1} Facture|[2,*] Factures',
    'invoice_creation' => 'Création d\'une Facture',
    'lastname' => '{1} Nom|[2,*] Noms',
    'link_to' => 'Lien vers :page',
    'login' => '{1} Identifiant|[2,*] Identifiants',
    'logout' => 'Se déconnecter',
    'member' => '{1} Adhérent|[2,*] Adhérents',
    'member_creation' => 'Création d\'un Adhérent',
    'member_deleted' => 'Adhérent Supprimé',
    'membership' => '{1} Adhésion|[2,*] Adhésions',
    'membership_creation' => 'Création d\'une Adhésion',
    'membership_of' => '{1} Adhésion de :user|[2,*] Adhésions de :user',
    'menu' => 'Menu',
    'my_account' => 'Mon Compte',
    'name' => 'Nom',
    'note' => 'Note',
    'payment-address' => '{1} Adresse de Paiement|[2,*] Adresses de Paiements',
    'payment_id' => '{1} N° Moyen de Paiement|[2,*] N° Moyens de Paiements',
    'payment-mean' => '{1} Moyen de Paiement|[2,*] Moyens de Paiements',
    'provider' => 'Fournisseur',
    'purchase-order' => '{1} Bon de Commande|[2,*] Bons de Commandes',
    'purchase-order_id' => '# Bon de commande',
    'quantity' => 'Quantité',
    'receipted-invoice' => '{1} Facture Acquittée|[2,*] Factures Acquittées',
    'receipted-invoice_creation' => 'Création d\'une Facture Acquittée',
    'research' => 'Rechercher',
    'role' => '{1} Rôle|[2,*] Rôles',
    'setting' => '{1} Paramètre|[2,*] Paramètres',
    'signatory_type' => "{1} Type du Signataire|[2,*] Types des Signataires",
    'signatory_name' => "{1} Nom du Signataire|[2,*] Noms des Signataires",
    'signatory_city' => "{1} Ville du Signataire|[2,*] Villes des Signataires",
    'total_amount' => '{1} Montant Total|[2,*] Montants Totaux',
    'type' => '{1} Type|[2,*] Types',
    'type_here' => 'Écrire ici',
    'unit_price' => 'Prix unitaire',
    'user' => '{1} Utilisateur|[2,*] Utilisateurs',
    'user_account' => '{1} Compte Utilisateur|[2,*] Comptes Utilisateurs',
    'user_creation' => 'Création d\'un Utilisateur',
    'username' => '{1} Nom d\'utilisateur|[2,*] Noms d\'utilisateurs',
    'VAT' => 'TVA',
    'website_settings' => 'Paramètres du site',



];
