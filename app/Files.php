<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'fileName',
        'filePath',
        'scholarship_id',
        'reason'
    ];

    public function scholarship(){
        return $this->belongsTo('App\Scholarship');
    }
}
