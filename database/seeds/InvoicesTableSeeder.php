<?php

use App\Invoice;
use Illuminate\Database\Seeder;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Invoice::query()->delete();

        Invoice::create([
            'id' => 17041131,
            'receipted' => true,
            'date' => '2017-04-11',
            'total_amount' => 25,
            'payment_mean' => "Espèces",
            'payment_address' => "Chez Nicolas",
            'payment_id' => NULL,
            'signatory_type' => "Président",
            'signatory_name' => "Jean Bon",
            'signatory_city' => "Nantes",
        ]);

        Invoice::create([
            'id' => 18041203,
            'receipted' => true,
            'date' => '2018-04-12',
            'total_amount' => 35.5,
            'payment_mean' => "Chèque",
            'payment_address' => "Chez Nicolas",
            'payment_id' => "18041204",
            'signatory_type' => "Trésorière",
            'signatory_name' => "Solen Quiniou",
            'signatory_city' => "Nantes",
        ]);

        Invoice::create([
            'id' => 18061312,
            'receipted' => false,
            'date' => '2018-06-13',
            'total_amount' => 95.9,
            'payment_mean' => "Bon de commande",
            'payment_address' => "Adr ATALA",
            'payment_id' => "18061313",
            'signatory_type' => "Trésorière",
            'signatory_name' => "Solen Quiniou",
            'signatory_city' => "Nantes",
        ]);

        Invoice::create([
            'id' => 19042014,
            'receipted' => true,
            'date' => '2019-04-20',
            'total_amount' => 35.5,
            'payment_mean' => "Chèque",
            'payment_address' => "Chez Nicolas",
            'payment_id' => "19042015",
            'signatory_type' => "Trésorière",
            'signatory_name' => "Solen Quiniou",
            'signatory_city' => "St-Nazaire",
        ]);

        Invoice::create([
            'id' => 19100116,
            'receipted' => false,
            'date' => '2019-10-01',
            'total_amount' => 50.0,
            'payment_mean' => "Bon de commande",
            'payment_address' => "IUT Nantes",
            'payment_id' => "19100117",
            'signatory_type' => "Trésorière",
            'signatory_name' => "Solen Quiniou",
            'signatory_city' => "Nantes",
        ]);

        Invoice::create([
            'id' => 19111407,
            'receipted' => false,
            'date' => '2019-11-14',
            'total_amount' => 45.9,
            'payment_mean' => "Bon de commande",
            'payment_address' => "LaboNantais",
            'payment_id' => "19111408",
            'signatory_type' => "Trésorière",
            'signatory_name' => "Solen Quiniou",
            'signatory_city' => "Nantes",
        ]);
    }
}
