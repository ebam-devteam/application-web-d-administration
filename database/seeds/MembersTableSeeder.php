<?php

use App\Member;
use Illuminate\Database\Seeder;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Member::query()->delete();

        Member::create([
            'lastname' => "Dubois",
            'firstname' => "Eliott",
            'login' => NULL,
            'email' => "eliott@gmail.com",
            'address' => "Chez Lui",
            'employer' => "IUT de Nantes"
        ]);

        Member::create([
            'lastname' => "Darry",
            'firstname' => "Nicolas",
            'login' => "SamaOs",
            'email' => NULL,
            'address' => "Chez Nicolas",
            'employer' => NULL
        ]);

        Member::create([
            'lastname' => "Dubois",
            'firstname' => "Robin",
            'login' => "desbois",
            'email' => NULL,
            'address' => NULL,
            'employer' => "Le peuple"
        ]);

        Member::create([
            'lastname' => "Naji",
            'firstname' => "Adame",
            'login' => "Hydrogen",
            'email' => "adame@gmail.com",
            'address' => NULL,
            'employer' => NULL
        ]);

        Member::create([
            'lastname' => "Batard",
            'firstname' => "Baptiste",
            'login' => NULL,
            'email' => "baptiste@gmail.com",
            'address' => NULL,
            'employer' => "Atala"
        ]);

        Member::create([
            'lastname' => "Quiniou",
            'firstname' => "Solen",
            'login' => NULL,
            'email' => "quiniou@univ-nantes.fr",
            'address' => NULL,
            'employer' => "Université de Nantes"
        ]);

        Member::create([
            'lastname' => "Sanchez",
            'firstname' => "Sandra",
            'login' => NULL,
            'email' => "sandra@outlook.com",
            'address' => "Chez Sandra",
            'employer' => "Chomeur"
        ]);

        Member::create([
            'lastname' => "Doe",
            'firstname' => "John",
            'login' => "noOne",
            'email' => NULL,
            'address' => NULL,
            'employer' => "Atala"
        ]);

        Member::create([
            'lastname' => "Le Sech",
            'firstname' => "Lise",
            'login' => NULL,
            'email' => NULL,
            'address' => NULL,
            'employer' => "IUT de Nantes"
        ]);

        Member::create([
            'lastname' => "Matin",
            'firstname' => "Martin",
            'login' => NULL,
            'email' => "martin@matin.com",
            'address' => "Chez Sandra",
            'employer' => "Atala"
        ]);
    }
}
