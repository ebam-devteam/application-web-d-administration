<?php

namespace App\Imports;

use App\Invoice;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class InvoicesImport implements ToModel, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Invoice([
            'id' => $row[0],
            'receipted' => $row[1],
            'date' => $row[2],
            'total_amount' => $row[3],
            'payment_mean' => $row[4],
            'payment_address' => $row[5],
            'payment_id' => $row[6],
            'signatory_type' => $row[7],
            'signatory_name' => $row[8],
            'signatory_city' => $row[9],
        ]);
    }

    public function getCsvSettings(): array
    {
        return [];
    }
}
