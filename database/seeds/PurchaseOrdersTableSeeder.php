<?php

use App\PurchaseOrder;
use Illuminate\Database\Seeder;

class PurchaseOrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PurchaseOrder::query()->delete();

        PurchaseOrder::create([
            'id' => 512,
            'provider' => "HP",
            'total_amount' => 2000,
            'VAT' => 400,
            'date' => '2018-10-8',
            'signatory_type' => "Tresorière",
            'signatory_name' => "Quiniou Solen",
            'signatory_city' => "Nantes"
        ]);

        PurchaseOrder::create([
            'id' => 652,
            'provider' => "Dell",
            'total_amount' => 5000,
            'VAT' => 1000,
            'date' => '2019-11-12',
            'signatory_type' => "Tresorière",
            'signatory_name' => "Quiniou Solen",
            'signatory_city' => "Nantes"
        ]);
    }
}
