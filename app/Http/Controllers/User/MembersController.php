<?php

namespace App\Http\Controllers\User;

use App\Exports\MembersExport;
use App\Imports\MembersImport;
use App\Http\Controllers\Controller;
use App\Member;
use App\Membership;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class MembersController extends Controller
{
    public function validator($request)
    {
        return $request->validate(
            [
                'lastname' => 'required',
                'firstname' => 'required',
                'login' => 'nullable',
                'email' => 'nullable',
                'address' => 'nullable',
                'employer' => 'nullable'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::all();
        return view('user.members.index')->with([
            'members' => $members,
            'count_value' => $members->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.members.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $member = new Member;
        return $this->update($request, $member);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        $memberships = Membership::where('member_id', $member->id)->get();

        return view('user.members.show')->with([
            'member' => $member,
            'memberships' => $memberships,
            'count_value' => $memberships->count()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        return view('user.members.edit')->with([
            'member' => $member,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $this->validator($request);

        $member->lastname = $request->lastname;
        $member->firstname = $request->firstname;
        $member->login = $request->login;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->employer = $request->employer;
        $member->save();

        return redirect()->route('user.members.show', $member->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        foreach ($member->memberships as $membership) {
            $membership->member()->dissociate();
        }
        $member->destroy($member->id);
        return redirect()->route('user.members.index');
    }

    /**
     * Download the resource collection in CSV format
     */
    public function export()
    {
        return Excel::download((new MembersExport()), 'members_'.date('ymdHis').'.csv');
    }

    /**
     * Import the resource in CSV format
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function import(Request $request)
    {
        if($request->hasFile('file')) {
            try {
                $path = $request->file('file')->getRealPath();
                Excel::import(new MembersImport, $path, null, \Maatwebsite\Excel\Excel::CSV);
            } catch (\Exception $e) {
                \Session::flash('error', $e->getMessage());
            }
        }
        return redirect()->route('user.members.index');
    }
}
