<?php

use App\DatalistMemberships;
use Illuminate\Database\Seeder;

class DatalistMembershipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DatalistMemberships::query()->delete();

        DatalistMemberships::create([
            'id' => 1,
            'type' => 'Etudiant / Sans emploi',
            'amount' => 20.0,
        ]);

        DatalistMemberships::create([
            'id' => 2,
            'type' => 'Etudiant',
            'amount' => 30.0,
        ]);

        DatalistMemberships::create([
            'id' => 3,
            'type' => 'Individuel',
            'amount' => 40.0,
        ]);

        DatalistMemberships::create([
            'id' => 4,
            'type' => 'Professionnel',
            'amount' => 60.0,
        ]);

        DatalistMemberships::create([
            'id' => 5,
            'type' => 'Institutionnel',
            'amount' => 200.0,
        ]);
    }
}
