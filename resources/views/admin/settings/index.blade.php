@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>{{ trans_choice('common.user_account', 2) }}</h1>
    </header>

    @can('manage')
        @include('admin.tabs')
    @endcan

    <main>
        <div class="lists">
            <datalist_type
              src_add_icon="{{ asset('images/plus_icon.svg') }}"
              src_del_icon="{{ asset('images/delete_icon.svg') }}"
              api_token="{{ Auth::user()->api_token }}">
            </datalist_type>
            <datalist_payment
              src_add_icon="{{ asset('images/plus_icon.svg') }}"
              src_del_icon="{{ asset('images/delete_icon.svg') }}"
              api_token="{{ Auth::user()->api_token }}">
            </datalist_payment>
        </div>
    </main>
</div>
@endsection
