@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @isset($membership->member)
                {{ trans_choice('common.membership_of', 1, ['user' => $membership->member->firstname." ".$membership->member->lastname]) }}
            @else
                @lang('common.membership_creation')
            @endisset
        </h1>
    </header>

    <main>
        <div class="db_infos">
            <form class="buttons_area" method="POST" accept-charset="utf-8">
                @csrf
                @if (Request::is('user/memberships/*/edit'))
                    @method('PATCH')
                @endif

                <autocomplete_members
                  error="@error('member_id') is-require @enderror"
                  label="{{ trans_choice('common.member', 1) }}"
                  resource="member"
                  value="{{ old('member_id', $membership->member_id ?? '') }}"
                  api_token="{{ Auth::user()->api_token }}">
                </autocomplete_members>
                @error('member_id')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <autocomplete_data_id
                  error="@error('invoice_id') is-require @enderror"
                  label="{{ trans_choice('common.invoice', 1) }}"
                  resource="invoice"
                  value="{{ old('invoice_id', $membership->invoice_id ?? '') }}"
                  api_token="{{ Auth::user()->api_token }}">
                </autocomplete_data_id>
                @error('invoice_id')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div class="case_infos">
                    <label for="date" class="title">{{ trans_choice('common.date', 1) }} : </label>
                    <input type="date" id="date" name="date" class="@error('date') is-require @enderror" value="{{ old('date', $membership->date ?? date('Y-m-d')) }}" required>
                </div>
                @error('date')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <autocomplete_data_type
                  error_amount="@error('amount') is-require @enderror"
                  error_type="@error('type_id') is-require @enderror"
                  label_amount="{{ trans_choice('common.amount', 1) }}"
                  label_type="{{ trans_choice('common.type', 1) }}"
                  resource="type"
                  value_amount="{{ old('amount', $membership->amount ?? '') }}"
                  value_type="{{ old('type', $membership->type ?? '') }}"
                  api_token="{{ Auth::user()->api_token }}">
                </autocomplete_data_type>
                @error('type_id')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                @error('amount')
                    <span role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <div>
                    <button type="submit" class="save" formaction="
                        @if (Request::is('user/memberships/*/edit'))
                            {{ route('user.memberships.update', $membership->id) }}
                        @else
                            {{ route('user.memberships.store') }}
                        @endif">
                        @lang('buttons.save')
                    </button>
                </div>
            </form>

            <form class="buttons_area" method="GET" accept-charset="utf-8">
                @csrf
                <button type="submit" class="cancel" formaction="
                    @if (Request::is('user/memberships/*/edit'))
                        {{ route('user.memberships.show', $membership->id) }}
                    @else
                        {{ route('user.memberships.index') }}
                    @endif">
                    @lang('buttons.cancel')
                </button>
            </form>
        </div>
    </main>
</div>
@endsection
