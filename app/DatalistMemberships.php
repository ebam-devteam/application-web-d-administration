<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatalistMemberships extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'type',
        'amount',
    ];
}
