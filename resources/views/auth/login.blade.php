@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="login-area">
        <div class="left-side">
            <img src="{{ asset('images/atala_logo_text.png') }}" alt="Association logo">
        </div>

        <div class="right-side">
            <form action="{{ route('login') }}" method="POST" accept-charset="utf-8">
                @csrf

                <h1>@lang('auth.signin')</h1>

                <input type="text" id="username" name="username" placeholder="@lang('auth.login')" class="@error('username') is-require @enderror" value="{{ old('login') }}" required autocomplete="username" autofocus>
                <label for="username"></label>

                <input type="password" id="password" name="password" placeholder="@lang('auth.password')" class="@error('password') is-require @enderror" required autocomplete="current-password">
                <label for="password"></label>

                @error('password')
                    <span role="alert"><strong>{{ $message }}</strong></span>
                @enderror

                @error('username')
                <span role="alert"><strong>{{ $message }}</strong></span>
                @enderror

                <div class="connection-option">
                    <div class="remerber-me">
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">@lang('auth.remember_me')</label>
                    </div>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">@lang('auth.forgot_password')</a>
                    @endif
                </div>

                <button type="submit" name="connection" class="btn btn-primary">@lang('auth.signin')</button>
            </form>
        </div>
    </div>
@endsection
