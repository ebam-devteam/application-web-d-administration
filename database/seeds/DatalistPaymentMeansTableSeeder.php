<?php

use App\DatalistPaymentMean;
use Illuminate\Database\Seeder;

class DatalistPaymentMeansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DatalistPaymentMean::query()->delete();

        DatalistPaymentMean::create([
            'id' => 1,
            'name' => 'Bon de commande',
        ]);

        DatalistPaymentMean::create([
            'id' => 2,
            'name' => 'Chèque',
        ]);

        DatalistPaymentMean::create([
            'id' => 3,
            'name' => 'Espèces',
        ]);

        DatalistPaymentMean::create([
            'id' => 4,
            'name' => 'Virement',
        ]);
    }
}
