@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{asset('css/home.css')}}" rel="stylesheet">
@endsection

@section('content')
        <div class="container_page">
            <header>
                <h1>@lang('common.menu')</h1>
            </header>

            <main>
                <section>
                    <div class="tiles">
                        <a href="{{ route('user.invoices.index') }}">
                            <img src="{{ asset('images/invoice_blue.svg') }}" alt="Invoice icon">
                            <p>{{ trans_choice('common.invoice', 2) }}</p>
                        </a>
                    </div>
                    <div class="tiles">
                        <a href="{{ route('user.receipted-invoices.index') }}">
                            <img src="{{ asset('images/receipted_invoice_blue.svg') }}" alt="Receipted Invoice icon">
                            <p>{{ trans_choice('common.receipted-invoice', 2) }}</p>
                        </a>
                    </div>
                    <div class="tiles">
                        <a href="{{ route('user.purchase-orders.index') }}">
                            <img src="{{ asset('images/purchase_order_blue.svg') }}" alt="Purchase Order icon">
                            <p>{{ trans_choice('common.purchase-order', 2) }}</p>
                        </a>
                    </div>
                </section>

                <div class="separation_line"></div>

                <section>
                    <div class="tiles">
                        <a href="{{ route('user.members.index') }}">
                            <img src="{{ asset('images/member_blue.svg') }}" alt="Member icon">
                            <p>{{ trans_choice('common.member', 2) }}</p>
                        </a>
                    </div>
                    <div class="tiles">
                        <a href="{{ route('user.memberships.index') }}">
                            <img src="{{ asset('images/membership_blue.svg') }}" alt="Membership icon">
                            <p>{{ trans_choice('common.membership', 2) }}</p>
                        </a>
                    </div>
                </section>

                <div class="separation_line"></div>

            </main>
        </div>
    </div>
</body>
</html>
@endsection
