@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
    <h1>{{ trans_choice('common.designation', 1)." : ".$designation->id}}</h1>
    </header>

    <main>
        <div class="details">
            <div>
                <p class="title">{{ trans_choice('common.purchase-order_id', 1) }} : </p>
                <p class="value">{{ $designation->purchase_order_id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.name', 1) }} : </p>
                <p class="value">{{ $designation->name }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.unit_price', 1) }} : </p>
                <p class="value">{{ $designation->unit_price }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.quantity', 1) }} : </p>
                <p class="value">{{ $designation->quantity }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.total_amount', 1) }} : </p>
                <p class="value">{{ $designation->total_amount }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.note', 1) }} : </p>
                <p class="value">{{ $designation->note }}</p>
            </div>


            <div class="options buttons_area">
                @can('edit')
                    <a href="{{ route('user.designations.edit', $designation->id) }}">
                        <button class="edit">
                            @lang('buttons.edit')
                            <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                        </button>
                    </a>
                @endcan

                @can('delete')
                    <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.designations.destroy', $designation->id) }}">
                        @csrf
                        @method('DELETE')
                        <confirm_action text="true" src="{{ asset('images/delete_icon.svg') }}">
                        </confirm_action>
                    </form>
                @endcan
            </div>
        </div>
    </main>
</div>
@endsection
