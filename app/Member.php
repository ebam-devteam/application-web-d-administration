<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'lastname',
        'firstname',
        'login',
        'email',
        'address',
        'employer'
    ];

    public function memberships() {
        return $this->hasMany('App\Membership');
    }
}
