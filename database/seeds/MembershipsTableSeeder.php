<?php

use App\Invoice;
use App\Member;
use App\Membership;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MembershipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Membership::truncate();

        Membership::create([
            'member_id' => 2,
            'invoice_id' => 17041131,
            'date' => '2017-04-11',
            'type' => "Etudiant",
            'amount' => 25
        ])
        ->member()->associate(Member::find(2))
        ->invoice()->associate(Invoice::find(17041131));

        Membership::create([
            'member_id' => 1,
            'invoice_id' => 19100116,
            'date' => '2019-10-01',
            'type' => "Etudiant",
            'amount' => 25
        ])
        ->member()->associate(Member::find(1))
        ->invoice()->associate(Invoice::find(19100116));

        Membership::create([
            'member_id' => 9,
            'invoice_id' => 19100116,
            'date' => '2019-10-01',
            'type' => "Etudiant",
            'amount' => 25
        ])
        ->member()->associate(Member::find(9))
        ->invoice()->associate(Invoice::find(19100116));

        Membership::create([
            'member_id' => 2,
            'invoice_id' => 18041203,
            'date' => '2018-04-12',
            'type' => "Salarié",
            'amount' => 35.5
        ])
        ->member()->associate(Member::find(2))
        ->invoice()->associate(Invoice::find(18041203));

        Membership::create([
            'member_id' => 3,
            'invoice_id' => 19111407,
            'date' => '2019-11-14',
            'type' => "Entrepreneur",
            'amount' => 45.9
        ])
        ->member()->associate(Member::find(3))
        ->invoice()->associate(Invoice::find(19111407));

        Membership::create([
            'member_id' => 5,
            'invoice_id' => 18061312,
            'date' => '2018-06-13',
            'type' => "Etudiant",
            'amount' => 25,
        ])
        ->member()->associate(Member::find(5))
        ->invoice()->associate(Invoice::find(18061312));

        Membership::create([
            'member_id' => 8,
            'invoice_id' => 18061312,
            'date' => '2018-06-13',
            'type' => "Chomeur",
            'amount' => 25,
        ])
        ->member()->associate(Member::find(8))
        ->invoice()->associate(Invoice::find(18061312));

        Membership::create([
            'member_id' => 10,
            'invoice_id' => 18061312,
            'date' => '2018-06-13',
            'type' => "Docteur",
            'amount' => 45.9,
        ])
        ->member()->associate(Member::find(10))
        ->invoice()->associate(Invoice::find(18061312));

        Membership::create([
            'member_id' => 2,
            'invoice_id' => 19042014,
            'date' => '2019-04-20',
            'type' => "Salarié",
            'amount' => 35.5,
        ])
        ->member()->associate(Member::find(2))
        ->invoice()->associate(Invoice::find(19042014));
    }
}
