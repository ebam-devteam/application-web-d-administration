<?php

namespace App\Exports;

use App\PurchaseOrder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class PurchaseOrdersExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return PurchaseOrder::all();
    }
    
    /**
    * @return array names of the header
    */
    public function headings(): array
    {
        return [
            'id',
            'provider',
            'total_amount',
            'VAT',
            'date',
            'signatory_type',
            'signatory_name',
            'signatory_city'
        ];
    }
}
