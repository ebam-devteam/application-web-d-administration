<nav>
    <div class="burger">
        <input type="checkbox" id="checkbox"/>
        <span></span>
        <span></span>
        <span></span>
    </div>

    <div class="menu" id="menu">
        <div class="user">
            <a href="{{ route('user.settings.show', Auth::user()->id) }}"><img src="{{ asset('images/option_white.svg') }}" alt="Settings" title="{{ trans_choice('common.setting', 2) }}"></a>
            <div>
                <div class="association-logo">
                    <img src="{{ asset('images/logo_atala.png') }}" alt="Settings">
                </div>
            </div>

            <p class="association-name">ATALA</p>
            <p>{{ Auth::user()->firstname." ".Auth::user()->lastname }}</p>
        </div>

        <span></span>

        <div class="links">
            <ul>
                <li><a href="{{ route('home') }}"><p>@lang('common.home')</p></a></li>
                <li><a href="{{ route('user.invoices.index') }}"><p>{{ trans_choice('common.invoice', 2) }}</p></a></li>
                <li><a href="{{ route('user.receipted-invoices.index') }}"><p>{{ trans_choice('common.receipted-invoice', 2) }}</p></a></li>
                <li><a href="{{ route('user.purchase-orders.index') }}"><p>{{ trans_choice('common.purchase-order', 2) }}</p></a></li>
                <li><a href="{{ route('user.members.index') }}"><p>{{ trans_choice('common.member', 2) }}</p></a></li>
                <li><a href="{{ route('user.memberships.index') }}"><p>{{ trans_choice('common.membership', 2) }}</p></a></li>
            </ul>
        </div>

        <span></span>

        <div class="disconnect_link">
            <form method="POST" accept-charset="utf-8" action="{{ route('logout') }}">
                @csrf
                <button type="submit">@lang('common.logout')</button>
            </form>
        </div>

        <div class="footer">
            <p>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></p>
        </div>
    </div>
</nav>
