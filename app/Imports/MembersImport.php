<?php

namespace App\Imports;

use App\Member;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class MembersImport implements ToModel, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Member([
            'id' => $row[0],
            'lastname' => $row[1],
            'firstname' => $row[2],
            'login' => $row[3],
            'email' => $row[4],
            'address' => $row[5],
            'employer' => $row[6],
        ]);
    }

    public function getCsvSettings(): array
    {
        return [];
    }
}
