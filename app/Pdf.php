<?php

namespace App;

use Codedge\Fpdf\Fpdf\Fpdf;

class Pdf extends Fpdf
{
    public function Header()
    {
        $this->Image(base_path().'/public/images/atala_logo.png',20,20,55);
        $this->SetFont('Arial','B',15);
        $this->SetMargins(20,20);
        $this->SetXY(110, 25);
        $this->MultiCell(80,7,'Association pour le Traitement Automatique des Langues', 0, 'R');
        $this->SetFont('Arial','',10);
        $this->SetXY(110, 40);
        $this->Cell(80,5,'45, rue d\'Ulm', 0, 2, 'R');
        $this->Cell(80,5,'75230 PARIS Cedex 05', 0, 2, 'R');
        $this->SetFont('Arial','B',10);
        $this->Cell(50,10,'SIRET 393 902 721 00017', 0, 2);
    }

    public function Header2()
    {
        $this->SetFont('Arial','B',10);
        $this->Cell(0,10, 'ATALA : Association pour le Traitement Automatique des LAngues', 0, 2);
        $this->SetFont('Arial','',10);
        $word = 'à';
        $word = iconv('UTF-8', 'windows-1252', $word);
        $this->Cell(0,0, 'Non astreint '.$word.' l\'inscription au registre de commerce, T.V.A. non applicable, article 293 B du CGI', 0, 2);
    }

    public function HeaderFacture($id)
    {
        $this->SetFont('Arial','B',16);
        $this->SetY(75);
        $word = 'Mémoire valant facture numéro '.$id.',';
        $word = iconv('UTF-8', 'windows-1252', $word);
        $this->Cell(0,15, $word, 0, 2);
    }

    public function BodyFacture($invoice, $memberships, $member)
    {
        $this->SetFont('Arial','',10);
        $this->SetY(125);
        $word = iconv('UTF-8', 'windows-1252', 'Mémoire');
        $this->Cell(30,0, $word, 0, 0);
        $this->Cell(0,0, 'DOIT :', 0, 0);

        $this->SetXY(65, 115);
        if ($invoice->receipted) {
            $pers = iconv('UTF-8', 'windows-1252', "$member->lastname $member->firstname");
            $this->Cell(0,4, $pers, 0, 2);
        } else {
            $this->Cell(0,4, 'Service comptable', 0, 2);
        }
        $employer = iconv('UTF-8', 'windows-1252', $member->employer);
        $this->Cell(0,4, $employer, 0, 2);
        $address = iconv('UTF-8', 'windows-1252', $invoice->payment_address);
        $this->Cell(0,4, $address, 0, 1);

        $this->SetY(140);
        $this->SetFont('');
        $sentence = iconv('UTF-8', 'windows-1252', "La somme de $invoice->total_amount € (".$this->float2alpha($invoice->total_amount)."), cotisation à l'ATALA pour l'année ".date('Y')." de : ");
        $this->MultiCell(0,4,$sentence, 0, 'J');

        $this->Cell(10,4, '', 0, 0);
        foreach ($memberships as $membership) {
            $text = iconv('UTF-8', 'windows-1252', "un adhérent tarif ".$membership->type." (".$membership->amount.") - ".$membership->member->lastname." ".$membership->member->firstname);
            $this->Cell(10,4, $text, 0, 2);
        }

        if ($invoice->payment_id) {
            $this->Cell(0,0, '', 0, 1);
            $this->Cell(61,4, 'correspondant au moyen de paiement ', 0, 0);
            $this->SetFont('', 'B');
            $text = iconv('UTF-8', 'windows-1252', $invoice->payment_mean);
            $this->Cell(40,4, $text, 0, 0);
            $this->SetFont('');
            $text = iconv('UTF-8', 'windows-1252', ' numéro ');
            $this->Cell(14,4, $text, 0, 0);
            $this->SetFont('', 'B');
            $this->Cell(0,4, $invoice->payment_id.'.', 0, 0);
        }
    }

    public function HeaderFactureAcquittee($id)
    {
        $this->SetFont('Arial','B',16);
        $this->SetY(75);
        $word = 'Mémoire valant facture acquittée numéro '.$id.',';
        $word = iconv('UTF-8', 'windows-1252', $word);
        $this->Cell(0,15, $word, 0, 2);
    }


    public function FooterFacture($villeS, $typeS, $nomS)
    {
        $this->SetXY(120, -100);
        $this->SetFont('Arial','',10);
        $this->Cell(50,60,$villeS.', le '.date('d/m/Y'), 0, 2);
        $this->Cell(50,5,'Pour l\'ATALA,', 0, 2);
        $typeS = iconv('UTF-8', 'windows-1252', $typeS);
        $this->Cell(50,5,$typeS.',', 0, 2);
        $this->Cell(50,5,$nomS, 0, 2);
    }


    function int2alpha($number, $root = true)
    {
        $number = (int)$number;
        $output = '';
        if($number >= 1000)
        {
            $num_arr = array();
            for($i = strlen("$number"); $i > 0; $i -= 3);
            {
                $j = ($i > 3) ? $i - 3 : 0;
                array_unshift($num_arr, substr("$number", $j, 3));
            }
            $num_arr = array_map(create_function('$a', 'return int2alpha($a, false);'), $num_arr);
            while(count($num_arr) > 0)
            {
                $output .= ' ' . array_shift($num_arr);
                if(count($num_arr) > 0)
                {
                    switch(count($num_arr) % 3)
                    {
                        case 1:
                            $output .= ' mille';
                            break;

                        case 2:
                            $output .= ' million';
                            break;

                        default:
                            $output .= ' milliard';
                    }
                }
            }
        }
        elseif($number >= 100)
        {
            $centaines = $this->int2alpha($number / 100);
            $reste = $this->int2alpha($number % 100);
            $output .= implode(' ', array(($centaines == 'un') ? 'cent' : "$centaines cent", $reste));
        }
        elseif($number > 80)
        {
            $number -= 80;
            $output .= 'quatre-vingt-' . $this->int2alpha($number);
        }
        elseif($number == 80)
        {
            $output .= 'quatre-vingt';
        }
        elseif($number == 71)
        {
            $output .= 'soixante et onze';
        }
        elseif($number > 61)
        {
            $number -= 60;
            $output .= 'soixante-' . $this->int2alpha($number);
        }
        elseif($number >= 20)
        {
            $unite = $number % 10;
            $dixaine = ($number-$unite) / 10;
            switch($dixaine)
            {
                case 2:
                    $output .= 'vingt';
                    break;

                case 3:
                    $output .= 'trente';
                    break;

                case 4:
                    $output .= 'quarante';
                    break;

                case 5:
                    $output .= 'cinquante';
                    break;

                case 6:
                    $output .= 'soixante';
                    break;
            }
            switch($unite)
            {
                case 0:
                    break;

                case 1:
                    $output .= ' et un';
                    break;

                default:
                    $output .= "-".$this->int2alpha($unite);
            }
        }
        elseif($number > 16)
        {
            $output = 'dix-'.$this->int2alpha($number % 10);
        }
        else
        {
            switch($number)
            {
                case 0:
                    if($root)
                        $output .= 'zéro';
                    else
                        $output .= '';
                    break;

                case 1:
                    $output .= 'un';
                    break;

                case 2:
                    $output .= 'deux';
                    break;

                case 3:
                    $output .= 'trois';
                    break;

                case 4:
                    $output .= 'quatre';
                    break;

                case 5:
                    $output .= 'cinq';
                    break;

                case 6:
                    $output .= 'six';
                    break;

                case 7:
                    $output .= 'sept';
                    break;

                case 8:
                    $output .= 'huit';
                    break;

                case 9:
                    $output .= 'neuf';
                    break;

                case 10:
                    $output .= 'dix';
                    break;

                case 11:
                    $output .= 'onze';
                    break;

                case 12:
                    $output .= 'douze';
                    break;

                case 13:
                    $output .= 'treize';
                    break;

                case 14:
                    $output .= 'quatorze';
                    break;

                case 15:
                    $output .= 'quinze';
                    break;

                case 16:
                    $output .= 'seize';
                    break;
            }
        }
        return $output;
    }

    function float2alpha($number)
    {
        $number *= 100;
        $number = (int) $number;
        $entier = $this->int2alpha($number / 100);
        $dec = $this->int2alpha($number % 100);
        if($dec == 'zéro')
            return "$entier euros";
        elseif($entier == 'zéro')
            return "$dec centimes d'euros";
        else
            return "$entier euros et $dec centimes";
    }
}
