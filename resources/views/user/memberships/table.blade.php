<input type="text" id="inputFilter" placeholder="@lang('common.research')" title="@lang('common.type_here')">

<div class="container_table">
    <div class="table" id="table">

        <div class="row header">
            @empty ($member)
                <div class="cell large">
                    <p>{{ trans_choice('common.member', $count_value) }}</p>
                </div>
            @endempty
            @empty (($invoice ?? '') || ($receipted_invoice ?? ''))
                <div class="cell short">
                    <p>{{ trans_choice('common.invoice', $count_value) }}</p>
                </div>
                <div class="cell short">
                    <p>{{ trans_choice('common.receipted-invoice', $count_value) }}</p>
                </div>
            @endempty
                <div class="cell medium">
                    <p>{{ trans_choice('common.date', $count_value) }}</p>
                </div>
                <div class="cell short">
                    <p>{{ trans_choice('common.type', $count_value) }}</p>
                </div>
                <div class="cell medium">
                    <p>{{ trans_choice('common.amount', $count_value) }}</p>
                </div>
            @can('edit', 'delete')
                <div class="cell options">
                    <p>{{ trans_choice('common.action', 2) }}</p>
                </div>
            @endcan
        </div>

        @foreach ($memberships as $membership)
            <div class="row">
                <a href="{{ route('user.memberships.show', $membership->id)}}">
                    @empty ($member)
                        <div class="cell large" data-title="{{ trans_choice('common.member', 1) }}">
                            <p>
                                @isset ($membership->member)
                                    {{ $membership->member->firstname." ".$membership->member->lastname }}
                                @endisset
                            </p>
                        </div>
                    @endempty
                    @empty (($invoice ?? '') || ($receipted_invoice ?? ''))
                        <div class="cell short" data-title="{{ trans_choice('common.invoice', 1) }}">
                            <p>
                                @isset($membership->invoice_id)
                                    @if (isset($membership->invoice) && !$membership->invoice->receipted)
                                        {{ $membership->invoice_id }}
                                    @endif
                                @endisset
                            </p>
                        </div>
                        <div class="cell short" data-title="{{ trans_choice('common.invoice', 1) }}">
                            <p>
                                @isset($membership->invoice_id)
                                    @if (isset($membership->invoice) && $membership->invoice->receipted)
                                        {{ $membership->invoice_id }}
                                    @endif
                                @endisset
                            </p>
                        </div>
                    @endempty
                    <div class="cell medium" data-title="{{ trans_choice('common.date', 1) }}">
                        <p>{{ $membership->date }}</p>
                    </div>
                    <div class="cell short" data-title="{{ trans_choice('common.type', 1) }}">
                        <p>{{ $membership->type }}</p>
                    </div>
                    <div class="cell medium" data-title="{{ trans_choice('common.amount', 1) }}">
                        <p>{{ $membership->amount }}</p>
                    </div>
                </a>

                @can('edit', 'delete')
                    <div class="cell options" data-title="{{ trans_choice('common.setting', 2) }}">
                        <div class="container-icon">
                            @can('edit')
                                <form class="ico" method="GET" accept-charset="utf-8" action="{{ route('user.memberships.edit', $membership->id) }}">
                                    @csrf
                                    <button type="submit" >
                                        <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                                    </button>
                                </form>
                            @endcan

                            @can('delete')
                                <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.memberships.destroy', $membership->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <confirm_action src="{{ asset('images/delete_icon.svg') }}">
                                    </confirm_action>
                                </form>
                            @endcan
                        </div>
                    </div>
                @endcan
            </div>
        @endforeach

    </div>
</div>
