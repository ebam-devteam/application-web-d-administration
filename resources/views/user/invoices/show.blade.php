@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            @if ($invoice->receipted)
                {{ trans_choice('common.receipted-invoice', 1) }}
            @else
                {{ trans_choice('common.invoice', 1) }}
            @endif
            {{ " : ".$invoice->id }}
        </h1>
    </header>

    <main>
        <div class="details">
            <div>
                <p class="title">
                    @if ($invoice->receipted)
                        {{ trans_choice('common.receipted-invoice', 1) }}
                    @else
                        {{ trans_choice('common.invoice', 1) }}
                    @endif
                     :
                </p>
                <p class="value">{{ $invoice->id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.date', 1) }} : </p>
                <p class="value">{{ $invoice->date }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.total_amount', 1) }} : </p>
                <p class="value">{{ $invoice->total_amount }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.payment-mean', 1) }} : </p>
                <p class="value">{{ $invoice->payment_mean }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.payment-address', 1) }} : </p>
                <p class="value">{{ $invoice->payment_address }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.payment_id', 1) }} : </p>
                <p class="value">{{ $invoice->payment_id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.signatory_type', 1) }} : </p>
                <p class="value">{{ $invoice->signatory_type }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.signatory_name', 1) }} : </p>
                <p class="value">{{ $invoice->signatory_name }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.signatory_city', 1) }} : </p>
                <p class="value">{{ $invoice->signatory_city }}</p>
            </div>

            <div class="options buttons_area">
                @can('edit')
                    <a href="
                        @if ($invoice->receipted)
                            {{ route('user.receipted-invoices.edit', $invoice->id) }}
                        @else
                            {{ route('user.invoices.edit', $invoice->id) }}
                        @endif">
                        <button class="edit">
                            @lang('buttons.edit')
                            <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                        </button>
                    </a>
                @endcan

                @can('read')
                    @if ($invoice->memberships->first())
                        <a target="_blank" href="
                            @if ($invoice->receipted)
                                {{ route('user.receipted-invoices.download', $invoice->id) }}
                            @else
                                {{ route('user.invoices.download', $invoice->id) }}
                            @endif">
                            @csrf
                            <button class="edit">
                                PDF
                                <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                            </button>
                        </a>
                    @endif
                @endcan

                @can('delete')
                    <form class="ico" method="POST" accept-charset="utf-8" action="
                            @if ($invoice->receipted)
                                {{ route('user.receipted-invoices.destroy', $invoice->id) }}
                            @else
                                {{ route('user.invoices.destroy', $invoice->id) }}
                            @endif
                        ">
                        @csrf
                        @method('DELETE')
                        <confirm_action text="true" src="{{ asset('images/delete_icon.svg') }}">
                        </confirm_action>
                    </form>
                @endcan
            </div>
        </div>

        <h2>{{ trans_choice('common.affected_membership', $count_value) }}</h2>

        @if ($invoice->receipted)
            @if ($count_value > 0)
                @include('user.memberships.details')
            @else
                <div class="buttons_area flex">
                    @can('edit')
                        <a href="{{ route('user.memberships.cf.invoice', $invoice->id) }}">
                            <button class="create">
                                @lang('buttons.add')
                                <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                            </button>
                        </a>
                    @endcan
                </div>
            @endif
        @else
            @if ($count_value > 0)
                @include('user.memberships.table')
            @endif

                <div class="buttons_area">
                    @can('edit')
                        <a href="{{ route('user.memberships.cf.invoice', $invoice->id) }}">
                            <button class="create">
                                @lang('buttons.add')
                                <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                            </button>
                        </a>
                    @endcan

                @if ($count_value > 0)
                    @can('read')
                        <a href="{{ route('user.memberships.export') }}">
                            <button class="export">
                                @lang('buttons.export')
                                <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                            </button>
                        </a>
                    @endcan
                @endif
            </div>
        @endif



    </main>
</div>
@endsection
