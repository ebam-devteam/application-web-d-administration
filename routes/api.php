<?php

use App\Http\Resources\DatalistMemberships as DatalistMembershipsResource;
use App\Http\Resources\Member as MemberResource;
use App\Http\Resources\Invoice as InvoiceResource;
use App\DatalistMemberships;
use App\Invoice;
use App\Member;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->group(function() {

    Route::get('/invoices', function () {
        return InvoiceResource::collection(Invoice::orderBy('id')->get());
    });

    Route::get('/members', function () {
        return MemberResource::collection(Member::orderBy('lastname')->get());
    });

    Route::get('/types', 'Admin\DatalistMembershipsController@index');
    Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function() {
        Route::apiResource('/datalistMemberships', 'DatalistMembershipsController');
    });

    Route::get('/payment_means', 'Admin\DatalistPaymentMeansController@index');
    Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage')->group(function() {
        Route::apiResource('/datalistPaymentMeans', 'DatalistPaymentMeansController');
    });

});
