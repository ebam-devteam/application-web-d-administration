@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>{{ trans_choice('common.purchase-order', 2) }}</h1>

    </header>

    <main>
        <input type="text" id="inputFilter" placeholder="@lang('common.research')" title="@lang('common.type_here')">

        <div class="container_table">
            <div class="table" id="table">
                <div class="row header">
                    <div class="cell short">
                        <p>{{ trans_choice('common.id', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.provider', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.total_amount', $count_value) }}</p>
                    </div>
                    <div class="cell short">
                        <p>{{ trans_choice('common.VAT', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.date', $count_value) }}</p>
                    </div>
                    <div class="cell large">
                        <p>{{ trans_choice('common.signatory_name', $count_value) }}</p>
                    </div>
                    @can('delete')
                    <div class="cell options">
                        <p>{{ trans_choice('common.action', 2) }}</p>
                    </div>
                    @endcan
                </div>
                @foreach ($purchaseOrders as $purchaseOrder)
                    <div class="row">
                        <a href="{{ route('user.purchase-orders.show', $purchaseOrder->id)}}">
                            <div class="cell short" data-title="{{ trans_choice('common.purchase-order', 1) }}">
                                <p>{{ $purchaseOrder->id}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.provider', 1) }}">
                                <p>{{ $purchaseOrder->provider}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.total_amount', 1) }}">
                                <p>{{ $purchaseOrder->total_amount}}</p>
                            </div>
                            <div class="cell short" data-title="{{ trans_choice('common.VAT', 1) }}">
                                <p>{{ $purchaseOrder->VAT}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.date', 1) }}">
                                <p>{{ $purchaseOrder->date}}</p>
                            </div>
                            <div class="cell large" data-title="{{ trans_choice('common.signatory_name', 1) }}">
                                <p>{{ $purchaseOrder->signatory_name}}</p>
                            </div>
                        </a>
                        @can('edit', 'delete')
                            <div class="cell options" data-title="{{ trans_choice('common.setting', 2) }}">
                                <div class="container-icon">
                                    @can('edit')
                                        <form class="ico" method="GET" accept-charset="utf-8" action="{{ route('user.purchase-orders.edit', $purchaseOrder->id) }}">
                                            @csrf
                                            <button type="submit" >
                                                <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                                            </button>
                                        </form>
                                    @endcan

                                    @can('delete')
                                        <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.purchase-orders.destroy', $purchaseOrder->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <confirm_action src="{{ asset('images/delete_icon.svg') }}">
                                            </confirm_action>
                                        </form>
                                    @endcan
                                </div>
                            </div>
                        @endcan

                    </div>
                @endforeach
            </div>
        </div>

        <div class="buttons_area">
            @can('edit')
                <a href="{{ route('user.purchase-orders.create') }}">
                    <button class="create">
                        @lang('buttons.add')
                        <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                    </button>
                </a>

                <div class="file_color">
                    <form method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <button class="import no-margin" type="submit" formaction="{{ route('user.purchase-orders.import') }}">
                            @lang('buttons.import')
                            <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                        </button>
                    </form>
                </div>
            @endcan

            @can('read')
                <a href="{{ route('user.purchase-orders.export') }}">
                    <button class="export">
                        @lang('buttons.export')
                        <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                    </button>
                </a>
            @endcan
        </div>

    </main>
</div>
@endsection
