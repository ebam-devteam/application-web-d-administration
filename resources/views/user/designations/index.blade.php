@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>{{ trans_choice('common.designation', 2) }}</h1>
    </header>

    <main>
        @include('user.designations.table')

        <div class="buttons_area">
            @can('edit')
                <a href="{{ route('user.designations.create') }}">
                    <button class="create">
                        @lang('buttons.add')
                        <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                    </button>
                </a>

            {{--    <div class="file_color">
                    <form method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <button class="import no-margin" type="submit" formaction="{{ route('user.designations.import') }}">
                            @lang('buttons.import')
                            <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                        </button>
                    </form>
                </div> --}}
            @endcan

    {{--    @can('read')
                <a href="{{ route('user.designations.export') }}">
                    <button class="export">
                        @lang('buttons.export')
                        <img src="{{ asset('images/download_icon.svg') }}" alt="Download icon">
                    </button>
                </a>
            @endcan --}}
        </div>

    </main>
</div>
@endsection
