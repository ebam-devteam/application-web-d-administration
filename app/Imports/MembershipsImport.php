<?php

namespace App\Imports;

use App\Membership;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class MembershipsImport implements ToModel, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Membership([
            'id' => $row[0],
            'member_id' => $row[1],
            'invoice_id' => $row[2],
            'date' => $row[3],
            'type' => $row[4],
            'amount' => $row[5],
        ]);
    }

    public function getCsvSettings(): array
    {
        return [];
    }
}
