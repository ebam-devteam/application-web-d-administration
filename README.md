<br>
<p align="center"><a href="https://iutnantes.univ-nantes.fr/"><img src="https://iutnantes.univ-nantes.fr/medias/photo/logoiutq_1377690591795.gif?ID_FICHE=627306" width="150"></a></p>
<br>

# Application Web d'Administration (AwA)
<br>

## À propos

Ce projet est réalisé par des élèves de l'[IUT de Nantes](https://iutnantes.univ-nantes.fr/) pour le compte d’[ATALA](https://www.atala.org/) (Association pour le Traitement Automatique des LAngues) qui est une association scientifique Française. Le but consiste à faciliter le travail de gestion de l’association, tant sur le plan économique qu’administratif. Il possède deux objectifs, le premier est de centraliser les informations des services proposés par l’association, le second est de permettre un gain de temps sur la gestion de la trésorerie. La réponse à ces besoins devra se faire via une application web. Si le projet concerne ATALA en premier lieu, il doit, dans un second temps, être pensé pour être utilisé par d’autres associations.

Actuellement, les services de l’association ne sont pas automatisés : c’est le trésorier de l’association qui s’occupe d’éditer manuellement les factures, les factures acquittées et les bons de commande. Il doit aussi s’occuper de la création du bilan financier. 
Il existe déjà de nombreuses solutions pour répondre aux besoins de gestion d’une association. Ce projet se distingue des autres en voulant proposer un service plus léger que les systèmes déjà existants et plus simple d’utilisation. De plus il est développé en priorité pour ATALA et sera ensuite modifié pour être utilisable pour d’autres associations.

<br>

## Documentations

### Frameworks
- [Laravel 6](https://laravel.com/docs/6.x/)
- [VueJS 2](https://vuejs.org/v2/guide/)

### Dépendances
- [FPDF](http://www.fpdf.org/)
- [Laravel Excel](https://docs.laravel-excel.com/3.1/getting-started/)
- [Laravel Localization](https://github.com/mcamara/laravel-localization#laravel-localization)

<br>

## Utilisation

### Pré-requis

- [PHP](https://www.php.net/)
- [Composer](https://getcomposer.org/)
- [npm](https://www.npmjs.com/)
- [MySQL](https://www.mysql.com/)
- [Apache2](https://doc.ubuntu-fr.org/apache2) (optionnel)

__N.B.__ Pour Windows, il est très conseillé d'installer [WAMP](http://www.wampserver.com/) qui regroupe déjà Apache, MySQL et PHP.

### Installation

1. Télécharger ou fork le projet depuis https://gitlab.com/ebam-devteam/application-web-d-administration
2. Exécuter `composer install`
3. Exécuter `npm install`
4. Copier le `.env.example.prod` ou `.env.example.dev` (selon les besoins) en `.env`
5. Configurer le `.env` selon votre configuration système
6. Exécuter `php artisan key:generate`
7. Exécuter `php artisan migrate --seed`
8. En environnement de développement __seulement__, exécuter `php artisan serve` pour avoir un serveur PHP (ou avoir un serveur PHP autre, comme Apache2)

__N.B.__ Deux comptes par défauts sont présents dans la base de données :

1.  - Nom d'utilisateur : **admin**
    - Mot de passe : **password**
2.  - Nom d'utilisateur : **membre**
    - Mot de passe : **password**

<br>

## License

Ce projet est sous licence MIT - voir le fichier [LICENCE](https://gitlab.com/ebam-devteam/application-web-d-administration/-/blob/master/LICENSE) pour plus de détails.
