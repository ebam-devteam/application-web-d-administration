<?php

namespace App\Exports;

use App\Invoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class InvoicesExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Invoice::where('receipted', '=', false)->get();
    }

    /**
     * @return array names of the header
     */
    public function headings(): array
    {
        return [
            'id',
            'receipted',
            'date',
            'total_amount',
            'payment_mean',
            'payment_address',
            'payment_id',
            'signatory_type',
            'signatory_name',
            'signatory_city',
            'created_at',
            'updated_at'
        ];
    }
}
