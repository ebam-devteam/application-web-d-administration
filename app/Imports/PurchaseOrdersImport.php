<?php

namespace App\Imports;

use App\PurchaseOrder;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class PurchaseOrdersImport implements ToModel, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new PurchaseOrder([
            'id' => $row[0],
            'provider' => $row[1],
            'total_amount' => $row[2],
            'VAT' => $row[3],
            'date' => $row[4],
            'signatory_type' => $row[5],
            'signatory_name' => $row[6],
            'signatory_city' => $row[7],
        ]);
    }

    public function getCsvSettings(): array
    {
        return [];
    }
}
