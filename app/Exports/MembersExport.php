<?php

namespace App\Exports;

use App\Member;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class MembersExport implements FromCollection, WithHeadings, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Member::all();
    }

    /**
     * @return array names of the header
     */
    public function headings(): array
    {
        return [
            'id',
            'lastname',
            'firstname',
            'login',
            'email',
            'address',
            'employer',
            'created_at',
            'updated_at'
        ];
    }
}
