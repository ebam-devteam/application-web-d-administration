<div class="linksForSettings">
    <ul>
        <li><a href="{{ route('user.settings.show', Auth::user()->id) }}"><p>@lang('common.my_account')</p></a></li>
        <li><a href="{{ route('admin.users.index') }}"><p>{{ trans_choice('common.user_account', 2) }}</p></a></li>
        <li><a href="{{ route('admin.settings.website') }}"><p>{{ trans_choice('common.website_settings', 2) }}</p></a></li>
    </ul>
</div>
