/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('autocomplete', require('./components/Autocomplete.vue').default);
Vue.component('autocomplete_data_id', require('./components/AutocompleteDataId.vue').default);
Vue.component('autocomplete_data_payment', require('./components/AutocompleteDataPayment.vue').default);
Vue.component('autocomplete_data_type', require('./components/AutocompleteDataType.vue').default);
Vue.component('autocomplete_members', require('./components/AutocompleteMembers.vue').default);
Vue.component('confirm_action', require('./components/ConfirmAction.vue').default);
Vue.component('datalist_type', require('./components/DatalistType.vue').default);
Vue.component('datalist_payment', require('./components/DatalistPayment.vue').default);
Vue.component('modal', require('./components/Modal.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: 'main',
});
