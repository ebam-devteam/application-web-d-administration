# Changelog

#### **Version 1.0.0** Released on April 17th 2020

First public release, only major additions are listed.
- Added encrypted authentication
- Added permissions with roles
- Added account settings
- Added website settings
- Added CRUD management for
  - invoices
  - receipted-invoices
  - purchase-orders
  - members
  - memberships
