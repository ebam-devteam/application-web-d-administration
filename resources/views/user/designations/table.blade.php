<input type="text" id="inputFilter" placeholder="@lang('common.research')" title="@lang('common.type_here')">

<div class="container_table">
    <div class="table" id="table">
        <div class="row header">
            <div class="cell short">
                <p>{{ trans_choice('common.id', $count_value) }}</p>
            </div>
            <div class="cell medium">
                <p>{{ trans_choice('common.name', $count_value) }}</p>
            </div>
            <div class="cell short">
                <p>{{ trans_choice('common.unit_price', $count_value) }}</p>
            </div>
            <div class="cell short">
                <p>{{ trans_choice('common.quantity', $count_value) }}</p>
            </div>
            <div class="cell medium">
                <p>{{ trans_choice('common.total_amount', $count_value) }}</p>
            </div>
            <div class="cell large">
                <p>{{ trans_choice('common.note', $count_value) }}</p>
            </div>
            @can('edit', 'delete')
                <div class="cell options">
                    <p>{{ trans_choice('common.action', 2) }}</p>
                </div>
            @endcan
        </div>
        @foreach ($designations as $designation)
            <div class="row">
                <a href="{{ route('user.designations.show', $designation->id)}}">
                    <div class="cell short" data-title="{{ trans_choice('common.purchase-order_id', 1) }}">
                        <p>{{ $designation->purchase_order_id}}</p>
                    </div>
                    <div class="cell medium" data-title="{{ trans_choice('common.name', 1) }}">
                        <p>{{ $designation->name}}</p>
                    </div>
                    <div class="cell short" data-title="{{ trans_choice('common.unit_price', 1) }}">
                        <p>{{ $designation->unit_price}}</p>
                    </div>
                    <div class="cell short" data-title="{{ trans_choice('common.quantity', 1) }}">
                        <p>{{ $designation->quantity}}</p>
                    </div>
                    <div class="cell medium" data-title="{{ trans_choice('common.total_amount', 1) }}">
                        <p>{{ $designation->total_amount}}</p>
                    </div>
                    <div class="cell large" data-title="{{ trans_choice('common.note', 1) }}">
                        <p>{{ $designation->note}}</p>
                    </div>
                </a>

                @can('edit', 'delete')
                    <div class="cell options" data-title="{{ trans_choice('common.setting', 2) }}">
                        <div class="container-icon">
                            @can('edit')
                                <form class="ico" method="GET" accept-charset="utf-8" action="{{ route('user.designations.edit', $designation->id) }}">
                                    @csrf
                                    <button type="submit" >
                                        <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                                    </button>
                                </form>
                            @endcan

                            @can('delete')
                                <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.designations.destroy', $designation->id) }}">
                                    @csrf
                                    @method('DELETE')
                                    <confirm_action src="{{ asset('images/delete_icon.svg') }}">
                                    </confirm_action>
                                </form>
                            @endcan
                        </div>
                    </div>
                @endcan
            </div>
        @endforeach
    </div>
</div>
