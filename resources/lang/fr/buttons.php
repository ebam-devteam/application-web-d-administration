<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Memberships Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used on memberships pages for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'example' => 'This sentence is the value for the key `example`.',
    'add' => 'Ajouter',
    'cancel' => 'Annuler',
    'delete' => 'Supprimer',
    'download' => 'Télécharger',
    'edit' => 'Editer',
    'export' => 'Exporter',
    'import' => 'Importer',
    'save' => 'Sauvegarder',

];
