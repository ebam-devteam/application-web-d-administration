<?php

namespace App\Http\Controllers\User;

use App\Exports\ReceiptedInvoicesExport;
use App\Imports\InvoicesImport;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Pdf;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReceiptedInvoicesController extends Controller
{
    public function validator($request)
    {
        return $request->validate(
            [
                'date' => 'required|date',
                'total_amount' => 'required|min:0',
                'payment_mean' => 'required',
                'payment_address' => 'required',
                'payment_id' => 'nullable',
                'signatory_type' => 'required',
                'signatory_name' => 'required',
                'signatory_city' => 'required'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $receiptedInvoices = Invoice::where('receipted', '=', true)->get();
        return view('user.invoices.index')->with([
            'invoices' => $receiptedInvoices,
            'count_value' => $receiptedInvoices->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return app()->call('App\Http\Controllers\User\InvoicesController@create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validator($request);

        $receiptedInvoice = new Invoice;
        $last = Invoice::max('id');
        if ($last / 100 == (int)date('ymd')) {
            $receiptedInvoice->id = $last +1;
        } else {
            $receiptedInvoice->id = (int)date('ymd')."00";
        }
        $receiptedInvoice->receipted = true;
        $receiptedInvoice->date = $request->date;
        $receiptedInvoice->total_amount = $request->total_amount;
        $receiptedInvoice->payment_mean = $request->payment_mean;
        $receiptedInvoice->payment_address = $request->payment_address;
        $receiptedInvoice->payment_id = $request->payment_id;
        $receiptedInvoice->signatory_type = $request->signatory_type;
        $receiptedInvoice->signatory_name = $request->signatory_name;
        $receiptedInvoice->signatory_city = $request->signatory_city;
        $receiptedInvoice->save();

        return redirect()->route('user.memberships.cf.invoice', $receiptedInvoice->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $receiptedInvoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $receiptedInvoice)
    {
        return app()->call('App\Http\Controllers\User\InvoicesController@show', ['invoice' => $receiptedInvoice]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $receiptedInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $receiptedInvoice)
    {
        return app()->call('App\Http\Controllers\User\InvoicesController@edit', ['invoice' => $receiptedInvoice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $receiptedInvoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $receiptedInvoice)
    {
        $this->validator($request);

        $receiptedInvoice->receipted = true;
        $receiptedInvoice->date = $request->date;
        $receiptedInvoice->total_amount = $request->total_amount;
        $receiptedInvoice->payment_mean = $request->payment_mean;
        $receiptedInvoice->payment_address = $request->payment_address;
        $receiptedInvoice->payment_id = $request->payment_id;
        $receiptedInvoice->signatory_type = $request->signatory_type;
        $receiptedInvoice->signatory_name = $request->signatory_name;
        $receiptedInvoice->signatory_city = $request->signatory_city;
        $receiptedInvoice->save();

        return redirect()->route('user.receipted-invoices.show', $receiptedInvoice->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $receiptedInvoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $receiptedInvoice)
    {
        if ($receiptedInvoice->membership) {
            $receiptedInvoice->membership->invoice()->dissociate();
        }
        $receiptedInvoice->destroy($receiptedInvoice->id);
        return redirect()->route('user.receipted-invoices.index');
    }

    /**
     * Download the resource collection in CSV format
     */
    public function export()
    {
        return Excel::download((new ReceiptedInvoicesExport()), 'receipted-invoices_'.date('ymdHis').'.csv');
    }

    /**
     * Import the resource in CSV format
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function import(Request $request)
    {
        if($request->hasFile('file')) {
            try {
                $path = $request->file('file')->getRealPath();
                Excel::import(new InvoicesImport, $path, null, \Maatwebsite\Excel\Excel::CSV);
            } catch (\Exception $e) {
                \Session::flash('error', $e->getMessage());
            }
        }
        return redirect()->route('user.receipted-invoices.index');
    }

    /**
     * Download a specific resource in PDF format
     *
     * @param  \App\Invoice  $receiptedInvoice
     */
    public function download(Invoice $receiptedInvoice)
    {
        $pdf = new Pdf();
        $pdf->AddPage();
        $pdf->HeaderFactureAcquittee($receiptedInvoice->id);
        $pdf->Header2();

        if ($receiptedInvoice->memberships) {
            $pdf->BodyFacture($receiptedInvoice, $receiptedInvoice->memberships, $receiptedInvoice->memberships->first()->member);

            $typeS = $receiptedInvoice->signatory_type;
            if (substr($typeS, -1) == 'e') {
                $typeS = 'La '.$typeS;
            } else {
                $typeS = 'Le '.$typeS;
            }
            $pdf->FooterFacture($receiptedInvoice->signatory_city, $receiptedInvoice->signatory_type, $receiptedInvoice->signatory_name);
            $pdf->Output();
        } else {
            return redirect('receipted-invoices/'.$receiptedInvoice->id);
        }

        exit;
    }
}
