<?php

namespace App\Http\Controllers\User;

use App\Exports\InvoicesExport;
use App\Imports\InvoicesImport;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Membership;
use App\Pdf;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InvoicesController extends Controller
{
    public function validator($request)
    {
        return $request->validate(
            [
                'date' => 'required|date',
                'total_amount' => 'required|min:0',
                'payment_mean' => 'required',
                'payment_address' => 'required',
                'payment_id' => 'nullable',
                'signatory_type' => 'required',
                'signatory_name' => 'required',
                'signatory_city' => 'required'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where('receipted', '=', false)->get();
        return view('user.invoices.index')->with([
            'invoices' => $invoices,
            'count_value' => $invoices->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.invoices.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoice = new Invoice;
        return $this->update($request, $invoice);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $membership = $invoice->memberships->first();
        $memberships = $invoice->memberships;

        return view('user.invoices.show')->with([
            'invoice' => $invoice,
            'membership' => $membership,
            'memberships' => $memberships,
            'count_value' => $memberships->count()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        return view('user.invoices.edit')->with([
            'invoice' => $invoice
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        $this->validator($request);

        if (empty($invoice->id)) {
            $last = Invoice::max('id');
            if ($last / 100 == (int)date('ymd')) {
                $invoice->id = $last +1;
            } else {
                $invoice->id = (int)date('ymd')."00";
            }
        }
        $invoice->receipted = false;
        $invoice->date = $request->date;
        $invoice->total_amount = $request->total_amount;
        $invoice->payment_mean = $request->payment_mean;
        $invoice->payment_address = $request->payment_address;
        $invoice->payment_id = $request->payment_id;
        $invoice->signatory_type = $request->signatory_type;
        $invoice->signatory_name = $request->signatory_name;
        $invoice->signatory_city = $request->signatory_city;
        $invoice->save();

        return redirect()->route('user.invoices.show', $invoice->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        foreach ($invoice->memberships as $membership) {
            $membership->invoice()->dissociate();
        }
        $invoice->destroy($invoice->id);
        return redirect()->route('user.invoices.index');
    }

    /**
     * Download the resource collection in CSV format
     */
    public function export()
    {
        return Excel::download((new InvoicesExport()), 'invoices_'.date('ymdHis').'.csv');
    }

    /**
     * Import the resource in CSV format
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function import(Request $request)
    {
        if($request->hasFile('file')) {
            try {
                $path = $request->file('file')->getRealPath();
                Excel::import(new InvoicesImport, $path, null, \Maatwebsite\Excel\Excel::CSV);
            } catch (\Exception $e) {
                \Session::flash('error', $e->getMessage());
            }
        }
        return redirect()->route('user.invoices.index');
    }

    /**
     * Download a specific resource in PDF format
     *
     * @param  \App\Invoice  $invoice
     */
    public function download(Invoice $invoice)
    {
        if ($invoice->memberships->first()) {
            $pdf = new Pdf();
            $pdf->AddPage();
            $pdf->HeaderFacture($invoice->id);
            $pdf->Header2();

            if ($invoice->memberships) {
                $pdf->BodyFacture($invoice, $invoice->memberships, $invoice->memberships->first()->member);

                $typeS = $invoice->signatory_type;
                if (substr($typeS, -1) == 'e') {
                    $typeS = 'La '.$typeS;
                } else {
                    $typeS = 'Le '.$typeS;
                }
                $pdf->FooterFacture($invoice->signatory_city, $invoice->signatory_type, $invoice->signatory_name);
                $pdf->Output();
            } else {
                return redirect('invoices/'.$invoice->id);
            }
            exit;
        }
        return redirect()->route('user.invoices.show', $invoice->id);
    }
}
