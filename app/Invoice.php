<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Invoice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'receipted',
        'date',
        'total_amount',
        'payment_mean',
        'payment_address',
        'payment_id',
        'signatory_type',
        'signatory_name',
        'signatory_city'
    ];

    public function memberships() {
        return $this->hasMany('App\Membership');
    }

    /**
     * Actualize the amount of the specified resource.
     */
    public function actualize_amount(float $added_amount)
    {
        $this->total_amount = $this->total_amount + $added_amount;
        $this->save();
    }
}
