@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            {{ trans_choice('common.my_account', 1) }}
        </h1>
    </header>

    @can('manage')
        @include('admin.tabs')
    @endcan

    <main>
        <div class="details">
            <div>
                <p class="title">{{ trans_choice('common.user', 1) }} : </p>
                <p class="value">{{ $user->id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.lastname', 1) }} : </p>
                <p class="value">{{ $user->lastname }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.firstname', 1) }} : </p>
                <p class="value">{{ $user->firstname }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.username', 1) }} : </p>
                <p class="value">{{ $user->username }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.email', 1) }} : </p>
                <p class="value">{{ $user->email }}</p>
            </div>

            <div class="options buttons_area">
                <a href="{{ route('user.settings.edit', $user->id) }}">
                    <button class="edit">
                        @lang('buttons.edit')
                        <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                    </button>
                </a>
            </div>

        </div>
    </main>
</div>
@endsection
