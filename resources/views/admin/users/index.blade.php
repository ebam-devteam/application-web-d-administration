@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>{{ trans_choice('common.user_account', 2) }}</h1>
    </header>

    @can('manage')
        @include('admin.tabs')
    @endcan

    <main>
        <input type="text" id="inputFilter" placeholder="@lang('common.research')" title="@lang('common.type_here')">

        <div class="container_table">
            <div class="table" id="table">
                <div class="row header">
                    <div class="cell short">
                        <p>{{ trans_choice('common.id', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.lastname', $count_value) }}</p>
                    </div>
                    <div class="cell medium">
                        <p>{{ trans_choice('common.firstname', $count_value) }}</p>
                    </div>
                    <div class="cell large">
                        <p>{{ trans_choice('common.username', $count_value) }}</p>
                    </div>
                    <div class="cell large">
                        <p>{{ trans_choice('common.email', $count_value) }}</p>
                    </div>
                    <div class="cell short">
                        <p>{{ trans_choice('common.role', 2) }}</p>
                    </div>
                    @can('edit', 'delete')
                        <div class="cell options">
                            <p>{{ trans_choice('common.action', 2) }}</p>
                        </div>
                    @endcan
                </div>
                @foreach ($users as $user)
                    <div class="row">
                        <a href="{{ route('admin.users.show', $user->id)}}">
                            <div class="cell short" data-title="{{ trans_choice('common.user', 1) }}">
                                <p>{{ $user->id}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.lastname', 1) }}">
                                <p>{{ $user->lastname}}</p>
                            </div>
                            <div class="cell medium" data-title="{{ trans_choice('common.firstname', 1) }}">
                                <p>{{ $user->firstname}}</p>
                            </div>
                            <div class="cell large" data-title="{{ trans_choice('common.username', 1) }}">
                                <p>{{ $user->username}}</p>
                            </div>
                            <div class="cell large" data-title="{{ trans_choice('common.email', 1) }}">
                                <p>{{ $user->email}}</p>
                            </div>
                            <div class="cell short" data-title="{{ trans_choice('common.role', count($user->roles()->get())) }}">
                                <p>{{ implode(',', $user->roles()->get()->pluck('name')->toArray()) }}</p>
                            </div>
                        </a>

                        @can('edit', 'delete')
                            <div class="cell options">
                                @can('edit')
                                    <form class="ico" method="GET" accept-charset="utf-8" action="{{ route('admin.users.edit', $user->id) }}">
                                        @csrf
                                        <button type="submit" >
                                            <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                                        </button>
                                    </form>
                                @endcan

                                @can('delete')
                                    <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('admin.users.destroy', $user->id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <confirm_action src="{{ asset('images/delete_icon.svg') }}">
                                        </confirm_action>
                                    </form>
                                @endcan
                            </div>
                        @endcan
                    </div>
                @endforeach
            </div>
        </div>

        <div class="buttons_area">
            @can('edit')
                <a href="{{ route('admin.users.create') }}">
                    <button class="create">
                        @lang('buttons.add')
                        <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                    </button>
                </a>
            @endcan
        </div>

    </main>
</div>
@endsection
