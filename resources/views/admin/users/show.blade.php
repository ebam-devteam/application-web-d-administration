@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
        <h1>
            {{ trans_choice('common.user_account', 1).' : '.$user->firstname.' '.$user->lastname }}
        </h1>
    </header>

    @can('manage')
        @include('admin.tabs')
    @endcan

    <main>
        <div class="details">
            <div>
                <p class="title">{{ trans_choice('common.user_account', 1) }} : </p>
                <p class="value">{{ $user->id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.lastname', 1) }} : </p>
                <p class="value">{{ $user->lastname }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.firstname', 1) }} : </p>
                <p class="value">{{ $user->firstname }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.username', 1) }} : </p>
                <p class="value">{{ $user->username }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.email', 1) }} : </p>
                <p class="value">{{ $user->email }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.role', count($user->roles()->get())) }} : </p>
                <p class="value">{{ implode(',', $user->roles()->get()->pluck('name')->toArray()) }}</p>
            </div>

            <div class="options buttons_area">
                @can('edit')
                    <a href="{{ route('admin.users.edit', $user->id) }}">
                        <button class="edit">
                            @lang('buttons.edit')
                            <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                        </button>
                    </a>
                @endcan
                @can('delete')
                    <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('admin.users.destroy', $user->id) }}">
                        @csrf
                        @method('DELETE')
                        <confirm_action src="{{ asset('images/delete_icon.svg') }}">
                        </confirm_action>
                    </form>
                @endcan
            </div>
        </div>
    </main>
</div>
@endsection
