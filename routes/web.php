<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$routes = ['only' => ['index', 'show', 'create', 'store', 'edit', 'update', 'destroy']];

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function() {
    Route::get('/', function () {
        return redirect()->route('login');
    });

    Auth::routes(['register' => false, 'reset' => false, 'verify' => false]);

    Route::get('/home', 'HomeController@index')->name('home');

    Route::middleware('auth')->group(function() {
        Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage')->group(function() {
            Route::resource('users', 'UsersController');
            Route::get('settings', 'UsersController@settings')->name('settings.website');
        });

        Route::namespace('User')->prefix('user')->name('user.')->group(function() {
            Route::get('settings', 'SettingsController@show')->name('settings.show');
            Route::get('settings/edit', 'SettingsController@edit')->name('settings.edit');
            Route::patch('settings', 'SettingsController@update')->name('settings.update');

            Route::middleware('can:edit')->group(function() {
                $edit = ['only' => ['create','store','edit','update']];

                Route::get('designations/{purchaseOrder}/cf-purchaseorder', 'DesignationsController@cf_purchaseorder')->name('designations.cf.purchase-order');
                Route::resource('designations', 'DesignationsController', $edit);
                Route::post('invoices/import', 'InvoicesController@import')->name('invoices.import');
                Route::resource('invoices', 'InvoicesController', $edit);
                Route::post('members/import', 'MembersController@import')->name('members.import');
                Route::resource('members', 'MembersController', $edit);
                Route::post('memberships/import', 'MembershipsController@import')->name('memberships.import');
                Route::resource('memberships', 'MembershipsController', $edit);
                Route::get('memberships/{invoice}/cf-invoice', 'MembershipsController@cf_invoice')->name('memberships.cf.invoice');
                Route::get('memberships/{member}/cf-member', 'MembershipsController@cf_member')->name('memberships.cf.member');
                Route::post('purchase-orders/import', 'PurchaseOrdersController@import')->name('purchase-orders.import');
                Route::resource('purchase-orders', 'PurchaseOrdersController', $edit);
                Route::post('receipted-invoices/import', 'ReceiptedInvoicesController@import')->name('receipted-invoices.import');
                Route::resource('receipted-invoices', 'ReceiptedInvoicesController', $edit);
            });

            Route::middleware('can:delete')->group(function() {
                $delete = ['only' => ['destroy']];

                Route::resource('designations', 'DesignationsController', $delete);
                Route::resource('invoices', 'InvoicesController', $delete);
                Route::resource('members', 'MembersController', $delete);
                Route::resource('memberships', 'MembershipsController', $delete);
                Route::resource('purchase-orders', 'PurchaseOrdersController', $delete);
                Route::resource('receipted-invoices', 'ReceiptedInvoicesController', $delete);
            });

            Route::middleware('can:read')->group(function() {
                $read = ['only' => ['index','show']];

                Route::resource('designations', 'DesignationsController', $read);
                Route::get('invoices/download/{invoice}', 'InvoicesController@download')->name('invoices.download');
                Route::get('invoices/export', 'InvoicesController@export')->name('invoices.export');
                Route::resource('invoices', 'InvoicesController', $read);
                Route::get('members/export', 'MembersController@export')->name('members.export');
                Route::resource('members', 'MembersController', $read);
                Route::get('memberships/export', 'MembershipsController@export')->name('memberships.export');
                Route::resource('memberships', 'MembershipsController', $read);
                Route::get('purchase-orders/export','PurchaseOrdersController@export')->name('purchase-orders.export');
                Route::resource('purchase-orders', 'PurchaseOrdersController', $read);
                Route::get('receipted-invoices/download/{receiptedInvoice}', 'ReceiptedInvoicesController@download')->name('receipted-invoices.download');
                Route::get('receipted-invoices/export', 'ReceiptedInvoicesController@export')->name('receipted-invoices.export');
                Route::resource('receipted-invoices', 'ReceiptedInvoicesController', $read);
            });
        });
    });
});
