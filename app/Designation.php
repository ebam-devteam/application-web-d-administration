<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'purchase_order_id',
        'name',
        'unit_price',
        'quantity',
        'total_amount',
        'note'
    ];

    public function purchase_order() {
        return $this->belongsTo('App\PurchaseOrder');
    }
}
