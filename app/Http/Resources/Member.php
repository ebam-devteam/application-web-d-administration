<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Member extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'login' => $this->login,
            'email' => $this->email,
            'address' => $this->address,
            'employer' => $this->employer,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
