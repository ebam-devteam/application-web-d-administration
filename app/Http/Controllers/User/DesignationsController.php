<?php

namespace App\Http\Controllers\User;

use App\PurchaseOrder;
use App\Designation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DesignationsController extends Controller
{
    public function validator($request)
    {
        return $request->validate(
            [
                //'id' => 'required',
                'purchase_order_id' => 'required',
                'name' => 'required',
                'unit_price' => 'required',
                'quantity' => 'required',
                'total_amount' => 'required',
                'note' => 'required'
            ]
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = Designation::all();
        return view('user.designations.index')->with([
            'designations' => $designations,
            'count_value' => $designations->count()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.designations.edit');
    }

    public function cf_purchaseorder(PurchaseOrder $purchaseOrder)
    {
        $designation = new Designation;
        $designation->purchase_order_id = $purchaseOrder->id;
        return view('user.designations.edit')->with([
            'designation' => $designation,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $designation = new Designation;
        return $this->update($request, $designation);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show(Designation $designation)
    {
        return view('user.designations.show')->with([
            'designation' => $designation,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        return view('user.designations.edit')->with([
            'designation' => $designation,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Designation $designation)
    {
        $this->validator($request);
        if (empty($designation->id)) {
            $designation->id = Designation::max('id') +1;
        }
        $designation->purchase_order_id = $request->purchase_order_id;
        $designation->name = $request->name;
        $designation->unit_price = $request->unit_price;
        $designation->quantity = $request->quantity;
        $designation->total_amount = $request->total_amount;
        $designation->note = $request->note;
        $designation->save();

        return redirect()->route('user.designations.show',$designation->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        $designation->destroy($designation->id);
        return redirect()->route('user.purchase-orders.show',$designation->purchase_order_id);
    }
}
