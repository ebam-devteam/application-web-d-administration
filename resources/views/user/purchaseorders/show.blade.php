@extends('layouts.default')

@section('javascript')
    {{-- Put here all the links to scripts --}}
    <script src="{{ mix('js/app.js') }}" async defer></script>
    <script type="text/javascript" src="{{ asset('js/tableFilter.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/tableSorter.js') }}"></script>
@endsection

@section('css')
    {{-- Put here all the links to stylesheet --}}
    <link href="{{ asset('css/buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/form.css') }}" rel="stylesheet">
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tables.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container_page">

    <header>
    <h1>{{ trans_choice('common.purchase-order', 1)." : ".$purchaseOrder->id}}</h1>
    </header>

    <main>
        <div class="details">
            <div>
                <p class="title">{{ trans_choice('common.purchase-order', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->id }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.provider', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->provider }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.total_amount', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->total_amount }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.VAT', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->VAT }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.date', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->date }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.signatory_type', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->signatory_type }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.signatory_name', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->signatory_name }}</p>
            </div>
            <div>
                <p class="title">{{ trans_choice('common.signatory_city', 1) }} : </p>
                <p class="value">{{ $purchaseOrder->signatory_city }}</p>
            </div>

            <div class="options buttons_area">
                @can('edit')
                    <a href="{{ route('user.purchase-orders.edit', $purchaseOrder->id) }}">
                        <button class="edit">
                            @lang('buttons.edit')
                            <img src="{{ asset('images/edit_icon.svg') }}" alt="Edit icon">
                        </button>
                    </a>
                @endcan

                @can('delete')
                    <form class="ico" method="POST" accept-charset="utf-8" action="{{ route('user.purchase-orders.destroy', $purchaseOrder->id) }}">
                        @csrf
                        @method('DELETE')
                        <confirm_action text="true" src="{{ asset('images/delete_icon.svg') }}">
                        </confirm_action>
                    </form>
                @endcan
            </div>
        </div>

        <h2>{{ trans_choice('common.affected_designation', $count_value) }}</h2>

        @if ($count_value > 0)
            @include('user.designations.table')
        @endif

        <div class="buttons_area flex">
            @can('edit')
                <a href="{{ route('user.designations.cf.purchase-order', $purchaseOrder->id) }}">
                    <button class="create">
                        @lang('buttons.add')
                        <img src="{{ asset('images/plus_icon.svg') }}" alt="Plus icon">
                    </button>
                </a>
            @endcan
        </div>
    </main>
</div>
@endsection
